<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\HttpFoundation\Response;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200819131444 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $pass = password_hash('password', PASSWORD_BCRYPT);
        $roles = json_encode(["ROLE_ADMIN"]);
        $this->addSql("INSERT INTO users (email, roles, password) VALUES ('admin@gmail.com', '" . $roles . "','" . $pass . "')");

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE users');

    }
}
