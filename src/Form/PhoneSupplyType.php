<?php

namespace App\Form;

use App\Entity\PhoneSupply;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PhoneSupplyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('imei')
            ->add('invoice_num')
            ->add('supplier')
            ->add('tax')
            ->add('price')
            ->add('date_delivery')
            ->add('date_invoice')
            ->add('date_delivery', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('date_invoice', DateType::class, [
                'widget' => 'single_text',
            ])
            ->add('count', IntegerType::class,array(
                'required' => false,
                'mapped' => false
            ))
            ->add('save', SubmitType::class, ['label' => 'Add'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PhoneSupply::class,
        ]);
    }
}
