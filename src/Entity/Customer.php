<?php

namespace App\Entity;

use App\Repository\CustomerRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CustomerRepository::class)
 */
class Customer extends PrestashopEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_default_group;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_lang;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $deleted;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $passwd;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $email;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_gender;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $birthday;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $newsletter;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $optin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $website;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $company;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $siret;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $ape;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $outstanding_allow_amount;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $show_public_prices;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_risk;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $max_payment_days;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $active;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $note;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_guest;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_shop;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_shop_group;

    /**
     * @ORM\OneToMany(targetEntity=Address::class, mappedBy="customer")
     */
    protected $addresses;

    /**
     * @ORM\OneToMany(targetEntity=Cart::class, mappedBy="customer")
     */
    protected $carts;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="customer")
     */
    protected $orders;

    public function __construct()
    {
        $this->addresses = new ArrayCollection();
        $this->carts = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return (int)$this->id;
    }

    public function getIdDefaultGroup(): ?int
    {
        return (int)$this->id_default_group;
    }

    public function setIdDefaultGroup(?int $id_default_group): self
    {
        $this->id_default_group = $id_default_group;

        return $this;
    }

    public function getIdLang(): ?int
    {
        return (int)$this->id_lang;
    }

    public function setIdLang(?int $id_lang): self
    {
        $this->id_lang = $id_lang;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(?bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getPasswd(): ?string
    {
        return $this->passwd;
    }

    public function setPasswd(string $passwd): self
    {
        $this->passwd = $passwd;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getIdGender(): ?int
    {
        return (int)$this->id_gender;
    }

    public function setIdGender(?int $id_gender): self
    {
        $this->id_gender = $id_gender;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(?\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getNewsletter(): ?bool
    {
        return $this->newsletter;
    }

    public function setNewsletter(?bool $newsletter): self
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    public function getOptin(): ?bool
    {
        return $this->optin;
    }

    public function setOptin(?bool $optin): self
    {
        $this->optin = $optin;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getSiret(): ?string
    {
        return $this->siret;
    }

    public function setSiret(?string $siret): self
    {
        $this->siret = $siret;

        return $this;
    }

    public function getApe(): ?string
    {
        return $this->ape;
    }

    public function setApe(?string $ape): self
    {
        $this->ape = $ape;

        return $this;
    }

    public function getOutstandingAllowAmount(): ?float
    {
        return $this->outstanding_allow_amount;
    }

    public function setOutstandingAllowAmount(?float $outstanding_allow_amount): self
    {
        $this->outstanding_allow_amount = $outstanding_allow_amount;

        return $this;
    }

    public function getShowPublicPrices(): ?bool
    {
        return $this->show_public_prices;
    }

    public function setShowPublicPrices(?bool $show_public_prices): self
    {
        $this->show_public_prices = $show_public_prices;

        return $this;
    }

    public function getIdRisk(): ?int
    {
        return (int)$this->id_risk;
    }

    public function setIdRisk(?int $id_risk): self
    {
        $this->id_risk = $id_risk;

        return $this;
    }

    public function getMaxPaymentDays(): ?int
    {
        return (int)$this->max_payment_days;
    }

    public function setMaxPaymentDays(?int $max_payment_days): self
    {
        $this->max_payment_days = $max_payment_days;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getIsGuest(): ?bool
    {
        return $this->is_guest;
    }

    public function setIsGuest(?bool $is_guest): self
    {
        $this->is_guest = $is_guest;

        return $this;
    }

    public function getIdShop(): ?int
    {
        return (int)$this->id_shop;
    }

    public function setIdShop(?int $id_shop): self
    {
        $this->id_shop = $id_shop;

        return $this;
    }

    public function getIdShopGroup(): ?int
    {
        return (int)$this->id_shop_group;
    }

    public function setIdShopGroup(?int $id_shop_group): self
    {
        $this->id_shop_group = $id_shop_group;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->firstname;
    }

    /**
     * @return Collection|Address[]
     */
    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    public function addAddress(Address $address): self
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses[] = $address;
            $address->setCustomer($this);
        }

        return $this;
    }

    public function removeAddress(Address $address): self
    {
        if ($this->addresses->removeElement($address)) {
            // set the owning side to null (unless already changed)
            if ($address->getCustomer() === $this) {
                $address->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cart[]
     */
    public function getCarts(): Collection
    {
        return $this->carts;
    }

    public function addCart(Cart $cart): self
    {
        if (!$this->carts->contains($cart)) {
            $this->carts[] = $cart;
            $cart->setCustomer($this);
        }

        return $this;
    }

    public function removeCart(Cart $cart): self
    {
        if ($this->carts->removeElement($cart)) {
            // set the owning side to null (unless already changed)
            if ($cart->getCustomer() === $this) {
                $cart->setCustomer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setCustomer($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getCustomer() === $this) {
                $order->setCustomer(null);
            }
        }

        return $this;
    }

    public function entitySchema()
    {
        return [
            'id', 'id_default_group', 'id_lang', 'deleted', 'passwd', 'lastname', 'firstname', 'email', 'id_gender', 'birthday',
            'newsletter', 'optin', 'website', 'company', 'siret', 'ape', 'outstanding_allow_amount', 'show_public_prices',
            'id_risk', 'max_payment_days', 'active', 'note', 'is_guest', 'id_shop', 'id_shop_group',
        ];
    }

    static public function getResourceName()
    {
        return "customer";
    }
}
