<?php

namespace App\Entity;

use App\Repository\StockRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StockRepository::class)
 */
class Stock extends PrestashopEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $id_product;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_product_attribute;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_shop;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_shop_group;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="stocks")
     */
    protected $product;

    /**
     * @ORM\ManyToOne(targetEntity=ProductAttribute::class, inversedBy="stocks")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $product_attribute;

    /**
     * @ORM\Column(type="integer")
     */
    protected $quantity;

    /**
     * @ORM\Column(type="integer")
     */
    protected $physical_quantity;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $depends_on_stock;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $out_of_stock;

    public function getId()
    {
        return $this->id;
    }

    public function getIdProduct(): ?int
    {
        return (int)$this->id_product;
    }

    public function setIdProduct(int $id_product): self
    {
        $this->id_product = $id_product;

        return $this;
    }

    public function getIdProductAttribute(): ?int
    {
        return (int)$this->id_product_attribute;
    }

    public function setIdProductAttribute(?int $id_product_attribute): self
    {
        $this->id_product_attribute = $id_product_attribute;

        return $this;
    }

    public function getIdShop(): ?int
    {
        return $this->id_shop;
    }

    public function setIdShop(?int $id_shop): self
    {
        $this->id_shop = $id_shop;

        return $this;
    }

    public function getIdShopGroup(): ?int
    {
        return $this->id_shop_group;
    }

    public function setIdShopGroup(?int $id_shop_group): self
    {
        $this->id_shop_group = $id_shop_group;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getProductAttribute(): ?ProductAttribute
    {
        return $this->product_attribute;
    }

    public function setProductAttribute(?ProductAttribute $product_attribute): self
    {
        $this->product_attribute = $product_attribute;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPhysicalQuantity(): ?int
    {
        return $this->physical_quantity;
    }

    public function setPhysicalQuantity(int $physical_quantity): self
    {
        $this->physical_quantity = $physical_quantity;

        return $this;
    }

    public function getDependsOnStock(): ?int
    {
        return $this->depends_on_stock;
    }

    public function setDependsOnStock(?int $depends_on_stock): self
    {
        $this->depends_on_stock = $depends_on_stock;

        return $this;
    }

    public function getOutOfStock(): ?int
    {
        return $this->out_of_stock;
    }

    public function setOutOfStock(?int $out_of_stock): self
    {
        $this->out_of_stock = $out_of_stock;

        return $this;
    }

    public function entitySchema()
    {
        return [
            'id',
            'product' => 'id_product',
            'product_attribute' => 'id_product_attribute',
            'id_shop',
            'id_shop_group', 'quantity', 'physical_quantity', 'depends_on_stock', 'out_of_stock'
        ];
    }

    static public function getResourceName()
    {
        return "stock_available";
    }

    public function __toString()
    {
        return (string)$this->getIdEntity();
    }
}
