<?php

namespace App\Entity;

use App\Repository\CarrierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CarrierRepository::class)
 */
class Carrier extends PrestashopEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $deleted;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_module;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_tax_rules_group;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_reference;

    /**
     * @ORM\Column(type="string", length=64)
     */
    protected $name;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $active;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_free;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $url;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $shipping_handling;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $shipping_external;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $range_behavior;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $shipping_method;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $max_width;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $max_height;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $max_depth;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $max_weight;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $grade;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    protected $external_module_name;

    /**
     * @ORM\Column(type="string", length=512)
     */
    protected $delay;

    /**
     * @ORM\OneToMany(targetEntity=Cart::class, mappedBy="carrier")
     */
    protected $carts;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="carrier")
     */
    protected $orders;

    public function __construct()
    {
        $this->carts = new ArrayCollection();
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return (int)$this->id;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(?bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getIsModule(): ?bool
    {
        return $this->is_module;
    }

    public function setIsModule(?bool $is_module): self
    {
        $this->is_module = $is_module;

        return $this;
    }

    public function getIdTaxRulesGroup(): ?int
    {
        return (int)$this->id_tax_rules_group;
    }

    public function setIdTaxRulesGroup(?int $id_tax_rules_group): self
    {
        $this->id_tax_rules_group = $id_tax_rules_group;

        return $this;
    }

    public function getIdReference(): ?int
    {
        return (int)$this->id_reference;
    }

    public function setIdReference(?int $id_reference): self
    {
        $this->id_reference = $id_reference;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getIsFree(): ?bool
    {
        return $this->is_free;
    }

    public function setIsFree(?bool $is_free): self
    {
        $this->is_free = $is_free;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getShippingHandling(): ?bool
    {
        return $this->shipping_handling;
    }

    public function setShippingHandling(?bool $shipping_handling): self
    {
        $this->shipping_handling = $shipping_handling;

        return $this;
    }

    public function getShippingExternal(): ?bool
    {
        return $this->shipping_external;
    }

    public function setShippingExternal(?bool $shipping_external): self
    {
        $this->shipping_external = $shipping_external;

        return $this;
    }

    public function getRangeBehavior(): ?bool
    {
        return $this->range_behavior;
    }

    public function setRangeBehavior(?bool $range_behavior): self
    {
        $this->range_behavior = $range_behavior;

        return $this;
    }

    public function getShippingMethod(): ?int
    {
        return (int)$this->shipping_method;
    }

    public function setShippingMethod(?int $shipping_method): self
    {
        $this->shipping_method = $shipping_method;

        return $this;
    }

    public function getMaxWidth(): ?float
    {
        return $this->max_width;
    }

    public function setMaxWidth(?float $max_width): self
    {
        $this->max_width = $max_width;

        return $this;
    }

    public function getMaxHeight(): ?float
    {
        return $this->max_height;
    }

    public function setMaxHeight(?float $max_height): self
    {
        $this->max_height = $max_height;

        return $this;
    }

    public function getMaxDepth(): ?float
    {
        return $this->max_depth;
    }

    public function setMaxDepth(?float $max_depth): self
    {
        $this->max_depth = $max_depth;

        return $this;
    }

    public function getMaxWeight(): ?float
    {
        return $this->max_weight;
    }

    public function setMaxWeight(?float $max_weight): self
    {
        $this->max_weight = $max_weight;

        return $this;
    }

    public function getGrade(): ?int
    {
        return (int)$this->grade;
    }

    public function setGrade(?int $grade): self
    {
        $this->grade = $grade;

        return $this;
    }

    public function getExternalModuleName(): ?string
    {
        return $this->external_module_name;
    }

    public function setExternalModuleName(?string $external_module_name): self
    {
        $this->external_module_name = $external_module_name;

        return $this;
    }

    public function getDelay(): ?string
    {
        return $this->delay;
    }

    public function setDelay(string $delay): self
    {
        $this->delay = $delay;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * @return Collection|Cart[]
     */
    public function getCarts(): Collection
    {
        return $this->carts;
    }

    public function addCart(Cart $cart): self
    {
        if (!$this->carts->contains($cart)) {
            $this->carts[] = $cart;
            $cart->setCarrier($this);
        }

        return $this;
    }

    public function removeCart(Cart $cart): self
    {
        if ($this->carts->removeElement($cart)) {
            // set the owning side to null (unless already changed)
            if ($cart->getCarrier() === $this) {
                $cart->setCarrier(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setCarrier($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getCarrier() === $this) {
                $order->setCarrier(null);
            }
        }

        return $this;
    }

    public function entitySchema()
    {
        return [
            'id', 'deleted', 'is_module', 'id_tax_rules_group', 'id_reference', 'name',
            'active', 'is_free', 'url', 'shipping_handling', 'shipping_external',
            'range_behavior', 'shipping_method', 'max_width', 'max_height', 'max_depth',
            'max_weight', 'grade', 'external_module_name', 'delay',
        ];
    }

    static public function getResourceName()
    {
        return "carrier";
    }
}
