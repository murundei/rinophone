<?php

namespace App\Entity;

use App\Repository\OrderStateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderStateRepository::class)
 */
class OrderState extends PrestashopEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $unremovable;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $delivery;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $hidden;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $send_email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $module_name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $invoice;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $color;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $logable;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $shipped;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $paid;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $pdf_delivery;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $pdf_invoice;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $deleted;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity=Address::class, mappedBy="state")
     */
    protected $addresses;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="orderState")
     */
    protected $orders;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isForPreparation=false;

    /**
     * @ORM\OneToMany(targetEntity=MarketPlaceCost::class, mappedBy="state")
     */
    private $marketPlaceCosts;

    public function __construct()
    {
        $this->addresses = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->marketPlaceCosts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return (int)$this->id;
    }

    public function getUnremovable(): ?bool
    {
        return $this->unremovable;
    }

    public function setUnremovable(?bool $unremovable): self
    {
        $this->unremovable = $unremovable;

        return $this;
    }

    public function getDelivery(): ?bool
    {
        return $this->delivery;
    }

    public function setDelivery(?bool $delivery): self
    {
        $this->delivery = $delivery;

        return $this;
    }

    public function getHidden(): ?bool
    {
        return $this->hidden;
    }

    public function setHidden(?bool $hidden): self
    {
        $this->hidden = $hidden;

        return $this;
    }

    public function getSendEmail(): ?bool
    {
        return $this->send_email;
    }

    public function setSendEmail(?bool $send_email): self
    {
        $this->send_email = $send_email;

        return $this;
    }

    public function getModuleName(): ?string
    {
        return $this->module_name;
    }

    public function setModuleName(?string $module_name): self
    {
        $this->module_name = $module_name;

        return $this;
    }

    public function getInvoice(): ?bool
    {
        return $this->invoice;
    }

    public function setInvoice(?bool $invoice): self
    {
        $this->invoice = $invoice;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getLogable(): ?bool
    {
        return $this->logable;
    }

    public function setLogable(?bool $logable): self
    {
        $this->logable = $logable;

        return $this;
    }

    public function getShipped(): ?bool
    {
        return $this->shipped;
    }

    public function setShipped(?bool $shipped): self
    {
        $this->shipped = $shipped;

        return $this;
    }

    public function getPaid(): ?bool
    {
        return $this->paid;
    }

    public function setPaid(?bool $paid): self
    {
        $this->paid = $paid;

        return $this;
    }

    public function getPdfDelivery(): ?bool
    {
        return $this->pdf_delivery;
    }

    public function setPdfDelivery(?bool $pdf_delivery): self
    {
        $this->pdf_delivery = $pdf_delivery;

        return $this;
    }

    public function getPdfInvoice(): ?bool
    {
        return $this->pdf_invoice;
    }

    public function setPdfInvoice(?bool $pdf_invoice): self
    {
        $this->pdf_invoice = $pdf_invoice;

        return $this;
    }

    public function getDeleted(): ?bool
    {
        return $this->deleted;
    }

    public function setDeleted(?bool $deleted): self
    {
        $this->deleted = $deleted;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * @return Collection|Address[]
     */
    public function getAddresses(): Collection
    {
        return $this->addresses;
    }

    public function addAddress(Address $address): self
    {
        if (!$this->addresses->contains($address)) {
            $this->addresses[] = $address;
            $address->setState($this);
        }

        return $this;
    }

    public function removeAddress(Address $address): self
    {
        if ($this->addresses->removeElement($address)) {
            // set the owning side to null (unless already changed)
            if ($address->getState() === $this) {
                $address->setState(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setOrderState($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getOrderState() === $this) {
                $order->setOrderState(null);
            }
        }

        return $this;
    }

    public function getIsForPreparation(): ?bool
    {
        return $this->isForPreparation;
    }

    public function setIsForPreparation(bool $isForPreparation): self
    {
        $this->isForPreparation = $isForPreparation;

        return $this;
    }

    public function entitySchema()
    {
        return [
            'id', 'unremovable', 'delivery', 'hidden', 'send_email', 'module_name', 'invoice',
            'color', 'logable', 'shipped', 'paid', 'pdf_delivery', 'pdf_invoice', 'deleted', 'name',
        ];
    }

    static public function getResourceName()
    {
        return "order_state";
    }

    /**
     * @return Collection|MarketPlaceCost[]
     */
    public function getmarketPlaceCosts(): Collection
    {
        return $this->marketPlaceCosts;
    }

    public function addmarketPlaceCost(MarketPlaceCost $marketPlaceCost): self
    {
        if (!$this->marketPlaceCosts->contains($marketPlaceCost)) {
            $this->marketPlaceCosts[] = $marketPlaceCost;
            $marketPlaceCost->setState($this);
        }

        return $this;
    }

    public function removemarketPlaceCost(MarketPlaceCost $marketPlaceCost): self
    {
        if ($this->marketPlaceCosts->removeElement($marketPlaceCost)) {
            // set the owning side to null (unless already changed)
            if ($marketPlaceCost->getState() === $this) {
                $marketPlaceCost->setState(null);
            }
        }

        return $this;
    }
}
