<?php

namespace App\Entity;

use App\Repository\OfflineSupplierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OfflineSupplierRepository::class)
 */
class OfflineSupplier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity=PhoneSupply::class, mappedBy="supplier")
     */
    private $phoneSupplies;

    public function __construct()
    {
        $this->phoneSupplies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|PhoneSupply[]
     */
    public function getPhoneSupplies(): Collection
    {
        return $this->phoneSupplies;
    }

    public function addPhoneSupply(PhoneSupply $phoneSupply): self
    {
        if (!$this->phoneSupplies->contains($phoneSupply)) {
            $this->phoneSupplies[] = $phoneSupply;
            $phoneSupply->setSupplier($this);
        }

        return $this;
    }

    public function removePhoneSupply(PhoneSupply $phoneSupply): self
    {
        if ($this->phoneSupplies->removeElement($phoneSupply)) {
            // set the owning side to null (unless already changed)
            if ($phoneSupply->getSupplier() === $this) {
                $phoneSupply->setSupplier(null);
            }
        }

        return $this;
    }

    public function __toString(){
        return (string)$this->getName();
    }
}
