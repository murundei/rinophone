<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order extends PrestashopEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $id_address_delivery;

    /**
     * @ORM\Column(type="integer")
     */
    protected $id_address_invoice;

    /**
     * @ORM\Column(type="integer")
     */
    protected $id_cart;

    /**
     * @ORM\Column(type="integer")
     */
    protected $id_currency;

    /**
     * @ORM\Column(type="integer")
     */
    protected $id_lang;

    /**
     * @ORM\Column(type="integer")
     */
    protected $id_customer;

    /**
     * @ORM\Column(type="integer")
     */
    protected $id_carrier;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $current_state;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $module;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $shipping_number;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_shop_group;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_shop;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $secure_key;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $payment;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $recyclable;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $gift;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $gift_message;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $mobile_theme;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $total_discounts;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $total_discounts_tax_incl;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $total_discounts_tax_excl;

    /**
     * @ORM\Column(type="float")
     */
    protected $total_paid;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $total_paid_tax_incl;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $total_paid_tax_excl;

    /**
     * @ORM\Column(type="float")
     */
    protected $total_paid_real;

    /**
     * @ORM\Column(type="float")
     */
    protected $total_products;

    /**
     * @ORM\Column(type="float")
     */
    protected $total_products_wt;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $total_shipping;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $total_shipping_tax_incl;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $total_shipping_tax_excl;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $carrier_tax_rate;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $total_wrapping;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $total_wrapping_tax_incl;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $total_wrapping_tax_excl;

    /**
     * @ORM\ManyToOne(targetEntity=Address::class, inversedBy="ordersDelivery")
     */
    protected $address_delivery;

    /**
     * @ORM\ManyToOne(targetEntity=Address::class, inversedBy="ordersInvoice")
     */
    protected $address_invoice;

    /**
     * @ORM\ManyToOne(targetEntity=Carrier::class, inversedBy="orders")
     */
    protected $carrier;

    /**
     * @ORM\ManyToOne(targetEntity=Cart::class, inversedBy="orders")
     */
    protected $cart;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="orders")
     */
    protected $customer;

    /**
     * @ORM\ManyToOne(targetEntity=OrderState::class, inversedBy="orders")
     */
    protected $orderState;

    /**
     * @ORM\OneToMany(targetEntity=OrderProduct::class, mappedBy="order")
     */
    private $orderProducts;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $reference;

    public function __construct()
    {
        $this->orderProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return (int)$this->id;
    }

    public function getIdAddressDelivery(): ?int
    {
        return (int)$this->id_address_delivery;
    }

    public function setIdAddressDelivery(int $id_address_delivery): self
    {
        $this->id_address_delivery = $id_address_delivery;

        return $this;
    }

    public function getIdAddressInvoice(): ?int
    {
        return (int)$this->id_address_invoice;
    }

    public function setIdAddressInvoice(int $id_address_invoice): self
    {
        $this->id_address_invoice = $id_address_invoice;

        return $this;
    }

    public function getIdCart(): ?int
    {
        return (int)$this->id_cart;
    }

    public function setIdCart(int $id_cart): self
    {
        $this->id_cart = $id_cart;

        return $this;
    }

    public function getIdCurrency(): ?int
    {
        return (int)$this->id_currency;
    }

    public function setIdCurrency(int $id_currency): self
    {
        $this->id_currency = $id_currency;

        return $this;
    }

    public function getIdLang(): ?int
    {
        return (int)$this->id_lang;
    }

    public function setIdLang(int $id_lang): self
    {
        $this->id_lang = $id_lang;

        return $this;
    }

    public function getIdCustomer(): ?int
    {
        return (int)$this->id_customer;
    }

    public function setIdCustomer(int $id_customer): self
    {
        $this->id_customer = $id_customer;

        return $this;
    }

    public function getIdCarrier(): ?int
    {
        return (int)$this->id_carrier;
    }

    public function setIdCarrier(int $id_carrier): self
    {
        $this->id_carrier = $id_carrier;

        return $this;
    }

    public function getCurrentState(): ?int
    {
        return (int)$this->current_state;
    }

    public function setCurrentState(?int $current_state): self
    {
        $this->current_state = $current_state;

        return $this;
    }

    public function getModule(): ?string
    {
        return $this->module;
    }

    public function setModule(string $module): self
    {
        $this->module = $module;

        return $this;
    }

    public function getShippingNumber(): ?string
    {
        return $this->shipping_number;
    }

    public function setShippingNumber(?string $shipping_number): self
    {
        $this->shipping_number = $shipping_number;

        return $this;
    }

    public function getIdShopGroup(): ?int
    {
        return (int)$this->id_shop_group;
    }

    public function setIdShopGroup(?int $id_shop_group): self
    {
        $this->id_shop_group = $id_shop_group;

        return $this;
    }

    public function getIdShop(): ?int
    {
        return (int)$this->id_shop;
    }

    public function setIdShop(?int $id_shop): self
    {
        $this->id_shop = $id_shop;

        return $this;
    }

    public function getSecureKey(): ?string
    {
        return $this->secure_key;
    }

    public function setSecureKey(?string $secure_key): self
    {
        $this->secure_key = $secure_key;

        return $this;
    }

    public function getPayment(): ?string
    {
        return $this->payment;
    }

    public function setPayment(string $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getRecyclable(): ?bool
    {
        return $this->recyclable;
    }

    public function setRecyclable(?bool $recyclable): self
    {
        $this->recyclable = $recyclable;

        return $this;
    }

    public function getGift(): ?bool
    {
        return $this->gift;
    }

    public function setGift(?bool $gift): self
    {
        $this->gift = $gift;

        return $this;
    }

    public function getGiftMessage(): ?string
    {
        return $this->gift_message;
    }

    public function setGiftMessage(?string $gift_message): self
    {
        $this->gift_message = $gift_message;

        return $this;
    }

    public function getMobileTheme(): ?bool
    {
        return $this->mobile_theme;
    }

    public function setMobileTheme(?bool $mobile_theme): self
    {
        $this->mobile_theme = $mobile_theme;

        return $this;
    }

    public function getTotalDiscounts(): ?float
    {
        return $this->total_discounts;
    }

    public function setTotalDiscounts(?float $total_discounts): self
    {
        $this->total_discounts = $total_discounts;

        return $this;
    }

    public function getTotalDiscountsTaxIncl(): ?float
    {
        return $this->total_discounts_tax_incl;
    }

    public function setTotalDiscountsTaxIncl(?float $total_discounts_tax_incl): self
    {
        $this->total_discounts_tax_incl = $total_discounts_tax_incl;

        return $this;
    }

    public function getTotalDiscountsTaxExcl(): ?float
    {
        return $this->total_discounts_tax_excl;
    }

    public function setTotalDiscountsTaxExcl(?float $total_discounts_tax_excl): self
    {
        $this->total_discounts_tax_excl = $total_discounts_tax_excl;

        return $this;
    }

    public function getTotalPaid(): ?float
    {
        return $this->total_paid;
    }

    public function setTotalPaid(float $total_paid): self
    {
        $this->total_paid = $total_paid;

        return $this;
    }

    public function getTotalPaidTaxIncl(): ?float
    {
        return $this->total_paid_tax_incl;
    }

    public function setTotalPaidTaxIncl(?float $total_paid_tax_incl): self
    {
        $this->total_paid_tax_incl = $total_paid_tax_incl;

        return $this;
    }

    public function getTotalPaidTaxExcl(): ?float
    {
        return $this->total_paid_tax_excl;
    }

    public function setTotalPaidTaxExcl(?float $total_paid_tax_excl): self
    {
        $this->total_paid_tax_excl = $total_paid_tax_excl;

        return $this;
    }

    public function getTotalPaidReal(): ?float
    {
        return $this->total_paid_real;
    }

    public function setTotalPaidReal(float $total_paid_real): self
    {
        $this->total_paid_real = $total_paid_real;

        return $this;
    }

    public function getTotalProducts(): ?float
    {
        return $this->total_products;
    }

    public function setTotalProducts(float $total_products): self
    {
        $this->total_products = $total_products;

        return $this;
    }

    public function getTotalProductsWt(): ?float
    {
        return $this->total_products_wt;
    }

    public function setTotalProductsWt(float $total_products_wt): self
    {
        $this->total_products_wt = $total_products_wt;

        return $this;
    }

    public function getTotalShipping(): ?float
    {
        return $this->total_shipping;
    }

    public function setTotalShipping(?float $total_shipping): self
    {
        $this->total_shipping = $total_shipping;

        return $this;
    }

    public function getTotalShippingTaxIncl(): ?float
    {
        return $this->total_shipping_tax_incl;
    }

    public function setTotalShippingTaxIncl(?float $total_shipping_tax_incl): self
    {
        $this->total_shipping_tax_incl = $total_shipping_tax_incl;

        return $this;
    }

    public function getTotalShippingTaxExcl(): ?float
    {
        return $this->total_shipping_tax_excl;
    }

    public function setTotalShippingTaxExcl(?float $total_shipping_tax_excl): self
    {
        $this->total_shipping_tax_excl = $total_shipping_tax_excl;

        return $this;
    }

    public function getCarrierTaxRate(): ?float
    {
        return $this->carrier_tax_rate;
    }

    public function setCarrierTaxRate(?float $carrier_tax_rate): self
    {
        $this->carrier_tax_rate = $carrier_tax_rate;

        return $this;
    }

    public function getTotalWrapping(): ?float
    {
        return $this->total_wrapping;
    }

    public function setTotalWrapping(?float $total_wrapping): self
    {
        $this->total_wrapping = $total_wrapping;

        return $this;
    }

    public function getTotalWrappingTaxIncl(): ?float
    {
        return $this->total_wrapping_tax_incl;
    }

    public function setTotalWrappingTaxIncl(?float $total_wrapping_tax_incl): self
    {
        $this->total_wrapping_tax_incl = $total_wrapping_tax_incl;

        return $this;
    }

    public function getTotalWrappingTaxExcl(): ?float
    {
        return $this->total_wrapping_tax_excl;
    }

    public function setTotalWrappingTaxExcl(?float $total_wrapping_tax_excl): self
    {
        $this->total_wrapping_tax_excl = $total_wrapping_tax_excl;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->shipping_number;
    }

    public function getAddressDelivery(): ?Address
    {
        return $this->address_delivery;
    }

    public function setAddressDelivery(?Address $address_delivery): self
    {
        $this->address_delivery = $address_delivery;

        return $this;
    }

    public function getAddressInvoice(): ?Address
    {
        return $this->address_invoice;
    }

    public function setAddressInvoice(?Address $address_invoice): self
    {
        $this->address_invoice = $address_invoice;

        return $this;
    }

    public function getCarrier(): ?Carrier
    {
        return $this->carrier;
    }

    public function setCarrier(?Carrier $carrier): self
    {
        $this->carrier = $carrier;

        return $this;
    }

    public function getCart(): ?Cart
    {
        return $this->cart;
    }

    public function setCart(?Cart $cart): self
    {
        $this->cart = $cart;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getOrderState(): ?OrderState
    {
        return $this->orderState;
    }

    public function setOrderState(?OrderState $orderState): self
    {
        $this->orderState = $orderState;

        return $this;
    }

    /**
     * @return Collection|OrderProduct[]
     */
    public function getOrderProducts(): Collection
    {
        return $this->orderProducts;
    }

    public function addOrderProduct(OrderProduct $orderProduct): self
    {
        if (!$this->orderProducts->contains($orderProduct)) {
            $this->orderProducts[] = $orderProduct;
            $orderProduct->setOrder($this);
        }

        return $this;
    }

    public function removeOrderProduct(OrderProduct $orderProduct): self
    {
        if ($this->orderProducts->removeElement($orderProduct)) {
            // set the owning side to null (unless already changed)
            if ($orderProduct->getOrder() === $this) {
                $orderProduct->setOrder(null);
            }
        }

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function clearPhoneSupplies(): self
    {
        $this->orderProducts->map(function ($orderProduct) {
            $orderProduct->clearPhoneSupplies();
        });
        return $this;
    }

    public function entitySchema()
    {
        return [
            'id', 'reference',
            'address_delivery' => 'id_address_delivery',
            'address_invoice' => 'id_address_invoice',
            'cart' => 'id_cart',
            'customer' => 'id_customer',
            'carrier' => 'id_carrier',
            'orderState' => 'current_state',
            'id_currency', 'id_lang',
            'module', 'shipping_number', 'id_shop_group', 'id_shop', 'secure_key', 'payment', 'recyclable', 'gift',
            'gift_message', 'mobile_theme', 'total_discounts', 'total_discounts_tax_incl', 'total_discounts_tax_excl', 'total_paid',
            'total_paid_tax_incl', 'total_paid_tax_excl', 'total_paid_real', 'total_products', 'total_products_wt', 'total_shipping',
            'total_shipping_tax_incl', 'total_shipping_tax_excl', 'carrier_tax_rate', 'total_wrapping', 'total_wrapping_tax_incl', 'total_wrapping_tax_excl'
        ];
    }


    static public function getResourceName()
    {
        return "orders";
    }
}
