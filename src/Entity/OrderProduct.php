<?php

namespace App\Entity;

use App\Repository\OrderProductRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Exception;
use ReflectionException;
use SimpleXMLElement;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrderProductRepository::class)
 */
class OrderProduct extends PrestashopEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="orderProducts")
     * @ORM\JoinColumn(nullable=true)
     */
    private $order;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="orderProducts")
     * @ORM\JoinColumn(nullable=true)
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity=ProductAttribute::class, inversedBy="orderProducts")
     */
    private $productAttribute;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $product_quantity;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $product_price;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $unit_price_tax_incl;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $unit_price_tax_excl;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $id_product;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $id_product_attribute;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $id_order;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $imeis = [];

    /**
     * @ORM\OneToMany(targetEntity=PhoneSupply::class, mappedBy="orderProduct")
     */
    private $phoneSupplies;

    public function __construct()
    {
        $this->phoneSupplies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getProductAttribute(): ?ProductAttribute
    {
        return $this->productAttribute;
    }

    public function setProductAttribute(?ProductAttribute $productAttribute): self
    {
        $this->productAttribute = $productAttribute;

        return $this;
    }

    public function getProductQuantity(): ?int
    {
        return $this->product_quantity;
    }

    public function setProductQuantity(int $product_quantity): self
    {
        $this->product_quantity = $product_quantity;

        return $this;
    }

    public function getProductPrice(): ?string
    {
        return $this->product_price;
    }

    public function setProductPrice(string $product_price): self
    {
        $this->product_price = $product_price;

        return $this;
    }

    public function getUnitPriceTaxIncl(): ?string
    {
        return $this->unit_price_tax_incl;
    }

    public function setUnitPriceTaxIncl(string $unit_price_tax_incl): self
    {
        $this->unit_price_tax_incl = $unit_price_tax_incl;

        return $this;
    }

    public function getUnitPriceTaxExcl(): ?string
    {
        return $this->unit_price_tax_excl;
    }

    public function setUnitPriceTaxExcl(string $unit_price_tax_excl): self
    {
        $this->unit_price_tax_excl = $unit_price_tax_excl;

        return $this;
    }

    public function getIdProduct(): ?int
    {
        return $this->id_product;
    }

    public function setIdProduct(int $product_id): self
    {
        $this->id_product = $product_id;

        return $this;
    }

    public function getIdProductAttribute(): ?int
    {
        return $this->id_product_attribute;
    }

    public function setIdProductAttribute(?int $product_attribute_id): self
    {
        $this->id_product_attribute = $product_attribute_id;

        return $this;
    }

    public function getIdOrder(): ?int
    {
        return $this->id_order;
    }

    public function setIdOrder(int $id_order): self
    {
        $this->id_order = $id_order;

        return $this;
    }


    public function getImeis(): ?array
    {
        return $this->imeis;
    }

    public function setImeis(?array $imeis): self
    {
        $this->imeis = $imeis;

        return $this;
    }

    /**
     * @return Collection|PhoneSupply[]
     */
    public function getPhoneSupplies(): Collection
    {
        return $this->phoneSupplies;
    }

    public function addPhoneSupply(PhoneSupply $phoneSupply): self
    {
        if (!$this->phoneSupplies->contains($phoneSupply)) {
            $this->phoneSupplies[] = $phoneSupply;
            $phoneSupply->setOrderProduct($this);
        }
        return $this;
    }

    public function setPhoneSupply(PhoneSupply $phoneSupply, $ind): self
    {
        if ($this->phoneSupplies[$ind]) {
            $this->removePhoneSupply($this->phoneSupplies[$ind]);
            $this->phoneSupplies[$ind] = $phoneSupply;
            $phoneSupply->setOrderProduct($this);
        } else $this->addPhoneSupply($phoneSupply);
        return $this;
    }

    public function clearPhoneSupplies(): self
    {
        $this->phoneSupplies->map(function ($phone) {
            $this->removePhoneSupply($phone);
        });
        return $this;
    }

    public function removePhoneSupply(PhoneSupply $phoneSupply): self
    {
        if ($this->phoneSupplies->removeElement($phoneSupply)) {
            // set the owning side to null (unless already changed)
            if ($phoneSupply->getOrderProduct() === $this) {
                $phoneSupply->setOrderProduct(null);
            }
        }
        return $this;
    }

    public function entitySchema()
    {
        return [
            'id',
            'product' => 'id_product',
            'order' => 'id_order',
            'productAttribute' => 'id_product_attribute',
            'unit_price_tax_incl',
            'unit_price_tax_excl', 'product_quantity', 'product_price',
        ];
    }

    static public function getResourceName()
    {
        return "order_detail";
    }

    public function __toString()
    {
        return (string)$this->getIdOrder();
    }

    public function unserialize($array): PrestashopEntity
    {
        $array = (object)$array;

        $this->id_entity = $array->id;
        $this->product_quantity = $array->product_quantity;
        $this->product_price = $array->product_price;
        $this->unit_price_tax_incl = $array->unit_price_tax_incl;
        $this->unit_price_tax_excl = $array->unit_price_tax_excl;
        $this->id_product = $array->product_id;
        $this->id_order = $array->id_order;
        $this->id_product_attribute = $array->product_attribute_id;
        $this->date_add = isset($array->date_add) ? new DateTime($array->date_add) :
            $this->date_add ? $this->date_add : new DateTime();
        $this->date_upd = isset($array->date_upd) ? new DateTime($array->date_upd) : new DateTime();


        return $this;
    }
}
