<?php

namespace App\Entity;

use App\Repository\PhoneSupplyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PhoneSupplyRepository::class)
 */
class PhoneSupply
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imei;

    /**
     * @ORM\Column(type="date")
     */
    private $date_delivery;

    /**
     * @ORM\Column(type="date")
     */
    private $date_invoice;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $invoice_num;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=OfflineSupplier::class, inversedBy="phoneSupplies")
     * @ORM\JoinColumn(nullable=false)
     */
    private $supplier;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_add;

    /**
     * @ORM\ManyToOne(targetEntity=OfflineTax::class, inversedBy="phoneSupplies")
     */
    private $tax;

    /**
     * @ORM\ManyToOne(targetEntity=OrderProduct::class, inversedBy="phoneSupplies")
     */
    private $orderProduct;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getImei(): ?string
    {
        return $this->imei;
    }

    public function setImei(string $imei): self
    {
        $this->imei = $imei;

        return $this;
    }

    public function getDateDelivery(): ?\DateTimeInterface
    {
        return $this->date_delivery;
    }

    public function setDateDelivery(\DateTimeInterface $date_delivery): self
    {
        $this->date_delivery = $date_delivery;

        return $this;
    }

    public function getDateInvoice(): ?\DateTimeInterface
    {
        return $this->date_invoice;
    }

    public function setDateInvoice(\DateTimeInterface $date_invoice): self
    {
        $this->date_invoice = $date_invoice;

        return $this;
    }

    public function getInvoiceNum(): ?string
    {
        return $this->invoice_num;
    }

    public function setInvoiceNum(string $invoice_num): self
    {
        $this->invoice_num = $invoice_num;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getSupplier(): ?OfflineSupplier
    {
        return $this->supplier;
    }

    public function setSupplier(?OfflineSupplier $supplier): self
    {
        $this->supplier = $supplier;

        return $this;
    }

    public function getDateAdd(): ?\DateTimeInterface
    {
        return $this->date_add;
    }

    public function setDateAdd(\DateTimeInterface $date_add): self
    {
        $this->date_add = $date_add;

        return $this;
    }

    public function getTax(): ?OfflineTax
    {
        return $this->tax;
    }

    public function setTax(?OfflineTax $tax): self
    {
        $this->tax = $tax;

        return $this;
    }

    public function __toString(){
        return (string)$this->getName();
    }

    public function getOrderProduct(): ?OrderProduct
    {
        return $this->orderProduct;
    }

    public function setOrderProduct(?OrderProduct $orderProduct): self
    {
        $this->orderProduct = $orderProduct;

        return $this;
    }
}
