<?php

namespace App\Entity;

use App\Repository\AddressRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AddressRepository::class)
 */
class Address extends PrestashopEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_customer;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_manufacturer;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_supplier;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_warehouse;

    /**
     * @ORM\Column(type="integer")
     */
    protected $id_country;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_state;

    /**
     * @ORM\Column(type="string", length=32)
     */
    protected $alias;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $company;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $lastname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $firstname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $vat_number;

    /**
     * @ORM\Column(type="string", length=128)
     */
    protected $address1;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $address2;

    /**
     * @ORM\Column(type="string", length=12, nullable=true)
     */
    protected $postcode;

    /**
     * @ORM\Column(type="string", length=64)
     */
    protected $city;

    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    protected $other;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $phone_mobile;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    protected $dni;

    /**
     * @ORM\ManyToOne(targetEntity=Manufacturer::class, inversedBy="addresses")
     */
    protected $manufacturer;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="addresses")
     */
    protected $customer;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="address_delivery")
     */
    protected $ordersDelivery;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="address_invoice")
     */
    protected $ordersInvoice;

    /**
     * @ORM\ManyToOne(targetEntity=Supplier::class, inversedBy="addresses")
     */
    protected $supplier;

    /**
     * @ORM\OneToMany(targetEntity=Cart::class, mappedBy="addressInvoice")
     */
    private $cartsInvoice;

    /**
     * @ORM\OneToMany(targetEntity=Cart::class, mappedBy="addressDelivery")
     */
    private $cartsDelivery;

    public function __construct()
    {
        $this->ordersDelivery = new ArrayCollection();
        $this->ordersInvoice = new ArrayCollection();
        $this->cartsInvoice = new ArrayCollection();
        $this->cartsDelivery = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return (int)$this->id;
    }

    public function getIdCustomer(): ?int
    {
        return (int)$this->id_customer;
    }

    public function setIdCustomer(?int $id_customer): self
    {
        $this->id_customer = $id_customer;

        return $this;
    }

    public function getIdManufacturer(): ?int
    {
        return (int)$this->id_manufacturer;
    }

    public function setIdManufacturer(?int $id_manufacturer): self
    {
        $this->id_manufacturer = $id_manufacturer;

        return $this;
    }

    public function getIdSupplier(): ?int
    {
        return (int)$this->id_supplier;
    }

    public function setIdSupplier(?int $id_supplier): self
    {
        $this->id_supplier = $id_supplier;

        return $this;
    }

    public function getIdWarehouse(): ?int
    {
        return (int)$this->id_warehouse;
    }

    public function setIdWarehouse(?int $id_warehouse): self
    {
        $this->id_warehouse = $id_warehouse;

        return $this;
    }

    public function getIdCountry(): ?int
    {
        return (int)$this->id_country;
    }

    public function setIdCountry(int $id_country): self
    {
        $this->id_country = $id_country;

        return $this;
    }

    public function getIdState(): ?int
    {
        return (int)$this->id_state;
    }

    public function setIdState(?int $id_state): self
    {
        $this->id_state = $id_state;

        return $this;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(?string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getVatNumber(): ?string
    {
        return $this->vat_number;
    }

    public function setVatNumber(?string $vat_number): self
    {
        $this->vat_number = $vat_number;

        return $this;
    }

    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    public function setAddress1(string $address1): self
    {
        $this->address1 = $address1;

        return $this;
    }

    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    public function setAddress2(?string $address2): self
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    public function setPostcode(?string $postcode): self
    {
        $this->postcode = $postcode;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getOther(): ?string
    {
        return $this->other;
    }

    public function setOther(?string $other): self
    {
        $this->other = $other;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPhoneMobile(): ?string
    {
        return $this->phone_mobile;
    }

    public function setPhoneMobile(?string $phone_mobile): self
    {
        $this->phone_mobile = $phone_mobile;

        return $this;
    }

    public function getDni(): ?string
    {
        return $this->dni;
    }

    public function setDni(?string $dni): self
    {
        $this->dni = $dni;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->firstname;
    }

    public function getManufacturer(): ?Manufacturer
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?Manufacturer $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrdersDelivery(): Collection
    {
        return $this->ordersDelivery;
    }

    public function addOrdersDelivery(Order $ordersDelivery): self
    {
        if (!$this->ordersDelivery->contains($ordersDelivery)) {
            $this->ordersDelivery[] = $ordersDelivery;
            $ordersDelivery->setAddressDelivery($this);
        }

        return $this;
    }

    public function removeOrdersDelivery(Order $ordersDelivery): self
    {
        if ($this->ordersDelivery->removeElement($ordersDelivery)) {
            // set the owning side to null (unless already changed)
            if ($ordersDelivery->getAddressDelivery() === $this) {
                $ordersDelivery->setAddressDelivery(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrdersInvoice(): Collection
    {
        return $this->ordersInvoice;
    }

    public function addOrdersInvoice(Order $ordersInvoice): self
    {
        if (!$this->ordersInvoice->contains($ordersInvoice)) {
            $this->ordersInvoice[] = $ordersInvoice;
            $ordersInvoice->setAddressInvoice($this);
        }

        return $this;
    }

    public function removeOrdersInvoice(Order $ordersInvoice): self
    {
        if ($this->ordersInvoice->removeElement($ordersInvoice)) {
            // set the owning side to null (unless already changed)
            if ($ordersInvoice->getAddressInvoice() === $this) {
                $ordersInvoice->setAddressInvoice(null);
            }
        }

        return $this;
    }

    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    public function setSupplier(?Supplier $supplier): self
    {
        $this->supplier = $supplier;
        $this->id_supplier = $supplier->getIdEntity();

        return $this;
    }

    public function entitySchema()
    {
        return [
            'id',
            'customer' => 'id_customer',
            'manufacturer' => 'id_manufacturer',
            'supplier' => 'id_supplier',
            'id_state',
            'id_warehouse', 'id_country',
            'alias', 'company', 'lastname', 'firstname', 'vat_number', 'address1',
            'address2', 'postcode', 'city', 'other', 'phone', 'phone_mobile', 'dni',
        ];
    }

    static public function getResourceName()
    {
        return "address";
    }

    /**
     * @return Collection|Cart[]
     */
    public function getCartsInvoice(): Collection
    {
        return $this->cartsInvoice;
    }

    public function addCartsInvoice(Cart $cartsInvoice): self
    {
        if (!$this->cartsInvoice->contains($cartsInvoice)) {
            $this->cartsInvoice[] = $cartsInvoice;
            $cartsInvoice->setAddressInvoice($this);
        }

        return $this;
    }

    public function removeCartsInvoice(Cart $cartsInvoice): self
    {
        if ($this->cartsInvoice->removeElement($cartsInvoice)) {
            // set the owning side to null (unless already changed)
            if ($cartsInvoice->getAddressInvoice() === $this) {
                $cartsInvoice->setAddressInvoice(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Cart[]
     */
    public function getCartsDelivery(): Collection
    {
        return $this->cartsDelivery;
    }

    public function addCartsDelivery(Cart $cartsDelivery): self
    {
        if (!$this->cartsDelivery->contains($cartsDelivery)) {
            $this->cartsDelivery[] = $cartsDelivery;
            $cartsDelivery->setAddressDelivery($this);
        }

        return $this;
    }

    public function removeCartsDelivery(Cart $cartsDelivery): self
    {
        if ($this->cartsDelivery->removeElement($cartsDelivery)) {
            // set the owning side to null (unless already changed)
            if ($cartsDelivery->getAddressDelivery() === $this) {
                $cartsDelivery->setAddressDelivery(null);
            }
        }

        return $this;
    }
}
