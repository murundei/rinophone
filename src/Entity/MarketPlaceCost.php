<?php

namespace App\Entity;

use App\Repository\MarketPlaceCostRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MarketPlaceCostRepository::class)
 */
class MarketPlaceCost
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=OrderState::class, inversedBy="marketPlaceCosts")
     */
    private $state;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $delivery_cost;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
     */
    private $work_cost;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2, nullable=true)
     */
    private $marketplace_percent;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getState(): ?OrderState
    {
        return $this->state;
    }

    public function setState(?OrderState $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getDeliveryCost(): ?string
    {
        return $this->delivery_cost;
    }

    public function setDeliveryCost(?string $delivery_cost): self
    {
        $this->delivery_cost = $delivery_cost;

        return $this;
    }

    public function getWorkCost(): ?string
    {
        return $this->work_cost;
    }

    public function setWorkCost(?string $work_cost): self
    {
        $this->work_cost = $work_cost;

        return $this;
    }

    public function getMarketplacePercent(): ?string
    {
        return $this->marketplace_percent;
    }

    public function setMarketplacePercent(?string $marketplace_percent): self
    {
        $this->marketplace_percent = $marketplace_percent;

        return $this;
    }

    public function __toString()
    {
       return (string)$this->getName();
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
