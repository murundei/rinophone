<?php

namespace App\Entity;

use App\Repository\CartRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CartRepository::class)
 */
class Cart extends PrestashopEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_address_delivery;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_address_invoice;

    /**
     * @ORM\Column(type="integer")
     */
    protected $id_currency;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_customer;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_guest;

    /**
     * @ORM\Column(type="integer")
     */
    protected $id_lang;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_shop_group;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_shop;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_carrier;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $recyclable;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $gift;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $gift_message;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $mobile_theme;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $delivery_option;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    protected $secure_key;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $allow_seperated_package;

    /**
     * @ORM\ManyToOne(targetEntity=Carrier::class, inversedBy="carts")
     */
    protected $carrier;

    /**
     * @ORM\ManyToOne(targetEntity=Customer::class, inversedBy="carts")
     */
    protected $customer;

    /**
     * @ORM\OneToMany(targetEntity=Order::class, mappedBy="cart")
     */
    protected $orders;

    /**
     * @ORM\ManyToOne(targetEntity=Address::class, inversedBy="cartsInvoice")
     */
    private $addressInvoice;

    /**
     * @ORM\ManyToOne(targetEntity=Address::class, inversedBy="cartsDelivery")
     */
    private $addressDelivery;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return (int)$this->id;
    }

    public function getIdAddressDelivery(): ?int
    {
        return (int)$this->id_address_delivery;
    }

    public function setIdAddressDelivery(?int $id_address_delivery): self
    {
        $this->id_address_delivery = $id_address_delivery;

        return $this;
    }

    public function getIdAddressInvoice(): ?int
    {
        return (int)$this->id_address_invoice;
    }

    public function setIdAddressInvoice(?int $id_address_invoice): self
    {
        $this->id_address_invoice = $id_address_invoice;

        return $this;
    }

    public function getIdCurrency(): ?int
    {
        return (int)$this->id_currency;
    }

    public function setIdCurrency(int $id_currency): self
    {
        $this->id_currency = $id_currency;

        return $this;
    }

    public function getIdCustomer(): ?int
    {
        return (int)$this->id_customer;
    }

    public function setIdCustomer(?int $id_customer): self
    {
        $this->id_customer = $id_customer;

        return $this;
    }

    public function getIdGuest(): ?int
    {
        return (int)$this->id_guest;
    }

    public function setIdGuest(?int $id_guest): self
    {
        $this->id_guest = $id_guest;

        return $this;
    }

    public function getIdLang(): ?int
    {
        return (int)$this->id_lang;
    }

    public function setIdLang(?int $id_lang): self
    {
        $this->id_lang = $id_lang;

        return $this;
    }

    public function getIdShopGroup(): ?int
    {
        return (int)$this->id_shop_group;
    }

    public function setIdShopGroup(?int $id_shop_group): self
    {
        $this->id_shop_group = $id_shop_group;

        return $this;
    }

    public function getIdShop(): ?int
    {
        return (int)$this->id_shop;
    }

    public function setIdShop(?int $id_shop): self
    {
        $this->id_shop = $id_shop;

        return $this;
    }

    public function getIdCarrier(): ?int
    {
        return (int)$this->id_carrier;
    }

    public function setIdCarrier(?int $id_carrier): self
    {
        $this->id_carrier = $id_carrier;

        return $this;
    }

    public function getRecyclable(): ?bool
    {
        return $this->recyclable;
    }

    public function setRecyclable(?bool $recyclable): self
    {
        $this->recyclable = $recyclable;

        return $this;
    }

    public function getGift(): ?bool
    {
        return $this->gift;
    }

    public function setGift(?bool $gift): self
    {
        $this->gift = $gift;

        return $this;
    }

    public function getGiftMessage(): ?string
    {
        return $this->gift_message;
    }

    public function setGiftMessage(?string $gift_message): self
    {
        $this->gift_message = $gift_message;

        return $this;
    }

    public function getMobileTheme(): ?bool
    {
        return $this->mobile_theme;
    }

    public function setMobileTheme(?bool $mobile_theme): self
    {
        $this->mobile_theme = $mobile_theme;

        return $this;
    }

    public function getDeliveryOption(): ?string
    {
        return $this->delivery_option;
    }

    public function setDeliveryOption(?string $delivery_option): self
    {
        $this->delivery_option = $delivery_option;

        return $this;
    }

    public function getSecureKey(): ?string
    {
        return $this->secure_key;
    }

    public function setSecureKey(?string $secure_key): self
    {
        $this->secure_key = $secure_key;

        return $this;
    }

    public function getAllowSeperatedPackage(): ?bool
    {
        return $this->allow_seperated_package;
    }

    public function setAllowSeperatedPackage(?bool $allow_seperated_package): self
    {
        $this->allow_seperated_package = $allow_seperated_package;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->gift;
    }

    public function getCarrier(): ?Carrier
    {
        return $this->carrier;
    }

    public function setCarrier(?Carrier $carrier): self
    {
        $this->carrier = $carrier;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setCart($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getCart() === $this) {
                $order->setCart(null);
            }
        }

        return $this;
    }

    public function getAddressInvoice(): ?Address
    {
        return $this->addressInvoice;
    }

    public function setAddressInvoice(?Address $addressInvoice): self
    {
        $this->addressInvoice = $addressInvoice;

        return $this;
    }

    public function getAddressDelivery(): ?Address
    {
        return $this->addressDelivery;
    }

    public function setAddressDelivery(?Address $addressDelivery): self
    {
        $this->addressDelivery = $addressDelivery;

        return $this;
    }

    public function entitySchema()
    {
        return [
            'id',
            'addressDelivery' => 'id_address_delivery',
            'addressInvoice' => 'id_address_invoice',
            'customer' => 'id_customer',
            'carrier' => 'id_carrier',
            'id_currency',
            'id_guest', 'id_lang', 'id_shop_group', 'id_shop',
            'recyclable',
            'gift', 'gift_message', 'mobile_theme', 'delivery_option', 'secure_key', 'allow_seperated_package',
        ];
    }

    static public function getResourceName()
    {
        return "cart";
    }
}
