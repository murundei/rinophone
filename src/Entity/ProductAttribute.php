<?php

namespace App\Entity;

use App\Repository\ProductAttributeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductAttributeRepository::class)
 */
class ProductAttribute extends PrestashopEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $id_product;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $location;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $ean13;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $isbn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $upc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $mpn;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $quantity;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $reference;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $supplier_reference;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $wholesale_price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $ecotax;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $weight;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $unit_price_impact;

    /**
     * @ORM\Column(type="integer")
     */
    protected $minimal_quantity;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $low_stock_threshold;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $low_stock_alert;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $default_on;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $available_date;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="productAttributes")
     */
    protected $product;

    /**
     * @ORM\OneToMany(targetEntity=OrderProduct::class, mappedBy="productAttribute")
     */
    private $orderProducts;

    /**
     * @ORM\OneToMany(targetEntity=Stock::class, mappedBy="product_attribute")
     */
    private $stocks;

    public function __construct()
    {
        $this->stocks = new ArrayCollection();
        $this->orderProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return (int)$this->id;
    }

    public function getIdProduct(): ?int
    {
        return (int)$this->id_product;
    }

    public function setIdProduct(int $id_product): self
    {
        $this->id_product = $id_product;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getEan13(): ?string
    {
        return $this->ean13;
    }

    public function setEan13(?string $ean13): self
    {
        $this->ean13 = $ean13;

        return $this;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(?string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getUpc(): ?string
    {
        return $this->upc;
    }

    public function setUpc(?string $upc): self
    {
        $this->upc = $upc;

        return $this;
    }

    public function getMpn(): ?string
    {
        return $this->mpn;
    }

    public function setMpn(?string $mpn): self
    {
        $this->mpn = $mpn;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return (int)$this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getSupplierReference(): ?string
    {
        return $this->supplier_reference;
    }

    public function setSupplierReference(?string $supplier_reference): self
    {
        $this->supplier_reference = $supplier_reference;

        return $this;
    }

    public function getWholesalePrice(): ?float
    {
        return $this->wholesale_price;
    }

    public function setWholesalePrice(?float $wholesale_price): self
    {
        $this->wholesale_price = $wholesale_price;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getEcotax(): ?float
    {
        return $this->ecotax;
    }

    public function setEcotax(?float $ecotax): self
    {
        $this->ecotax = $ecotax;

        return $this;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(?float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getUnitPriceImpact(): ?float
    {
        return $this->unit_price_impact;
    }

    public function setUnitPriceImpact(?float $unit_price_impact): self
    {
        $this->unit_price_impact = $unit_price_impact;

        return $this;
    }

    public function getMinimalQuantity(): ?int
    {
        return (int)$this->minimal_quantity;
    }

    public function setMinimalQuantity(int $minimal_quantity): self
    {
        $this->minimal_quantity = $minimal_quantity;

        return $this;
    }

    public function getLowStockThreshold(): ?int
    {
        return (int)$this->low_stock_threshold;
    }

    public function setLowStockThreshold(?int $low_stock_threshold): self
    {
        $this->low_stock_threshold = $low_stock_threshold;

        return $this;
    }

    public function getLowStockAlert(): ?bool
    {
        return $this->low_stock_alert;
    }

    public function setLowStockAlert(?bool $low_stock_alert): self
    {
        $this->low_stock_alert = $low_stock_alert;

        return $this;
    }

    public function getDefaultOn(): ?bool
    {
        return $this->default_on;
    }

    public function setDefaultOn(?bool $default_on): self
    {
        $this->default_on = $default_on;

        return $this;
    }

    public function getAvailableDate(): ?\DateTimeInterface
    {
        return $this->available_date;
    }

    public function setAvailableDate(?\DateTimeInterface $available_date): self
    {
        $this->available_date = $available_date;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->reference;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return Collection|OrderProduct[]
     */
    public function getOrderProducts(): Collection
    {
        return $this->orderProducts;
    }

    public function addOrderProduct(OrderProduct $orderProduct): self
    {
        if (!$this->orderProducts->contains($orderProduct)) {
            $this->orderProducts[] = $orderProduct;
            $orderProduct->setProductAttribute($this);
        }

        return $this;
    }

    public function removeOrderProduct(OrderProduct $orderProduct): self
    {
        if ($this->orderProducts->removeElement($orderProduct)) {
            // set the owning side to null (unless already changed)
            if ($orderProduct->getProductAttribute() === $this) {
                $orderProduct->setProductAttribute(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Stock[]
     */
    public function getStocks(): Collection
    {
        return $this->stocks;
    }

    public function addStock(Stock $stock): self
    {
        if (!$this->stocks->contains($stock)) {
            $this->stocks[] = $stock;
            $stock->setProductAttribute($this);
        }

        return $this;
    }

    public function removeStock(Stock $stock): self
    {
        if ($this->stocks->removeElement($stock)) {
            // set the owning side to null (unless already changed)
            if ($stock->getProductAttribute() === $this) {
                $stock->setProductAttribute(null);
            }
        }

        return $this;
    }

    public function entitySchema()
    {
        return [
            'id',
            'product' => 'id_product',
            'location', 'ean13', 'isbn', 'upc', 'mpn', 'quantity',
            'reference', 'supplier_reference', 'wholesale_price', 'price', 'ecotax',
            'weight', 'unit_price_impact', 'minimal_quantity', 'low_stock_threshold',
            'low_stock_alert', 'default_on', 'available_date',
        ];
    }

    static public function getResourceName()
    {
        return "product_attribute";
    }
}