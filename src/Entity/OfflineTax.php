<?php

namespace App\Entity;

use App\Repository\OfflineTaxRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OfflineTaxRepository::class)
 */
class OfflineTax
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $percent;

    /**
     * @ORM\OneToMany(targetEntity=PhoneSupply::class, mappedBy="tax")
     */
    private $phoneSupplies;

    /**
     * @ORM\OneToOne(targetEntity=Tax::class, cascade={"persist", "remove"})
     */
    private $tax;

    public function __construct()
    {
        $this->phoneSupplies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPercent(): ?string
    {
        return $this->percent;
    }

    public function setPercent(string $percent): self
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * @return Collection|PhoneSupply[]
     */
    public function getPhoneSupplies(): Collection
    {
        return $this->phoneSupplies;
    }

    public function addPhoneSupply(PhoneSupply $phoneSupply): self
    {
        if (!$this->phoneSupplies->contains($phoneSupply)) {
            $this->phoneSupplies[] = $phoneSupply;
            $phoneSupply->setTax($this);
        }

        return $this;
    }

    public function removePhoneSupply(PhoneSupply $phoneSupply): self
    {
        if ($this->phoneSupplies->removeElement($phoneSupply)) {
            // set the owning side to null (unless already changed)
            if ($phoneSupply->getTax() === $this) {
                $phoneSupply->setTax(null);
            }
        }

        return $this;
    }

    public function __toString(){
        return (string)$this->getName();
    }

    public function getTax(): ?Tax
    {
        return $this->tax;
    }

    public function setTax(?Tax $tax): self
    {
        $this->tax = $tax;

        return $this;
    }
}
