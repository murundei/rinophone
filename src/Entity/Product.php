<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product extends PrestashopEntity
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_manufacturer;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_supplier;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $id_category_default;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $reference;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $supplier_reference;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $location;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $width;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $height;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $depth;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $weight;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $quantity_discount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $ean13;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $isbn;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $upc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $mpn;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $cache_is_pack;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $is_virtual;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $state;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $additional_delivery_times;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $delivery_in_stock;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $delivery_out_stock;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $on_sale;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $online_only;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $ecotax;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $minimal_quantity;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $low_stock_threshold;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $low_stock_alert;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $price;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $wholesale_price;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $unity;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    protected $additional_shipping_cost;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $customizable;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $active;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $redirect_type;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $indexed;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    protected $meta_description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $meta_keywords;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $meta_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $link_rewrite;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    protected $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description_short;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $available_now;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $available_later;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class, mappedBy="products")
     */
    protected $categories;

    /**
     * @ORM\ManyToOne(targetEntity=Manufacturer::class, inversedBy="products")
     */
    protected $manufacturer;

    /**
     * @ORM\OneToMany(targetEntity=ProductAttribute::class, mappedBy="product")
     */
    protected $productAttributes;

    /**
     * @ORM\ManyToOne(targetEntity=Supplier::class, inversedBy="products")
     */
    protected $supplier;

    /**
     * @ORM\OneToMany(targetEntity=OrderProduct::class, mappedBy="product")
     */
    private $orderProducts;

    /**
     * @ORM\OneToMany(targetEntity=Stock::class, mappedBy="product")
     */
    private $stocks;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->productAttributes = new ArrayCollection();
        $this->orderProducts = new ArrayCollection();
        $this->stocks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return (int)$this->id;
    }

    public function getIdManufacturer(): ?int
    {
        return (int)$this->id_manufacturer;
    }

    public function setIdManufacturer(?int $id_manufacturer): self
    {
        $this->id_manufacturer = $id_manufacturer;

        return $this;
    }

    public function getIdSupplier(): ?int
    {
        return (int)$this->id_supplier;
    }

    public function setIdSupplier(?int $id_supplier): self
    {
        $this->id_supplier = $id_supplier;

        return $this;
    }

    public function getIdCategoryDefault(): ?int
    {
        return (int)$this->id_category_default;
    }

    public function setIdCategoryDefault(?int $id_category_default): self
    {
        $this->id_category_default = $id_category_default;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(?string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getSupplierReference(): ?string
    {
        return $this->supplier_reference;
    }

    public function setSupplierReference(?string $supplier_reference): self
    {
        $this->supplier_reference = $supplier_reference;

        return $this;
    }

    public function getLocation(): ?string
    {
        return $this->location;
    }

    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    public function getWidth(): ?float
    {
        return $this->width;
    }

    public function setWidth(?float $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getHeight(): ?float
    {
        return $this->height;
    }

    public function setHeight(?float $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getDepth(): ?float
    {
        return $this->depth;
    }

    public function setDepth(?float $depth): self
    {
        $this->depth = $depth;

        return $this;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(?float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getQuantityDiscount(): ?bool
    {
        return $this->quantity_discount;
    }

    public function setQuantityDiscount(?bool $quantity_discount): self
    {
        $this->quantity_discount = $quantity_discount;

        return $this;
    }

    public function getEan13(): ?string
    {
        return $this->ean13;
    }

    public function setEan13(?string $ean13): self
    {
        $this->ean13 = $ean13;

        return $this;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(?string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getUpc(): ?string
    {
        return $this->upc;
    }

    public function setUpc(?string $upc): self
    {
        $this->upc = $upc;

        return $this;
    }

    public function getMpn(): ?string
    {
        return $this->mpn;
    }

    public function setMpn(?string $mpn): self
    {
        $this->mpn = $mpn;

        return $this;
    }

    public function getCacheIsPack(): ?bool
    {
        return $this->cache_is_pack;
    }

    public function setCacheIsPack(?bool $cache_is_pack): self
    {
        $this->cache_is_pack = $cache_is_pack;

        return $this;
    }

    public function getIsVirtual(): ?bool
    {
        return $this->is_virtual;
    }

    public function setIsVirtual(?bool $is_virtual): self
    {
        $this->is_virtual = $is_virtual;

        return $this;
    }

    public function getState(): ?int
    {
        return (int)$this->state;
    }

    public function setState(?int $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getAdditionalDeliveryTimes(): ?int
    {
        return (int)$this->additional_delivery_times;
    }

    public function setAdditionalDeliveryTimes(?int $additional_delivery_times): self
    {
        $this->additional_delivery_times = $additional_delivery_times;

        return $this;
    }

    public function getDeliveryInStock(): ?string
    {
        return $this->delivery_in_stock;
    }

    public function setDeliveryInStock(?string $delivery_in_stock): self
    {
        $this->delivery_in_stock = $delivery_in_stock;

        return $this;
    }

    public function getDeliveryOutStock(): ?string
    {
        return $this->delivery_out_stock;
    }

    public function setDeliveryOutStock(?string $delivery_out_stock): self
    {
        $this->delivery_out_stock = $delivery_out_stock;

        return $this;
    }

    public function getOnSale(): ?bool
    {
        return $this->on_sale;
    }

    public function setOnSale(?bool $on_sale): self
    {
        $this->on_sale = $on_sale;

        return $this;
    }

    public function getOnlineOnly(): ?bool
    {
        return $this->online_only;
    }

    public function setOnlineOnly(?bool $online_only): self
    {
        $this->online_only = $online_only;

        return $this;
    }

    public function getEcotax(): ?float
    {
        return $this->ecotax;
    }

    public function setEcotax(?float $ecotax): self
    {
        $this->ecotax = $ecotax;

        return $this;
    }

    public function getMinimalQuantity(): ?int
    {
        return (int)$this->minimal_quantity;
    }

    public function setMinimalQuantity(?int $minimal_quantity): self
    {
        $this->minimal_quantity = $minimal_quantity;

        return $this;
    }

    public function getLowStockThreshold(): ?int
    {
        return (int)$this->low_stock_threshold;
    }

    public function setLowStockThreshold(?int $low_stock_threshold): self
    {
        $this->low_stock_threshold = $low_stock_threshold;

        return $this;
    }

    public function getLowStockAlert(): ?bool
    {
        return $this->low_stock_alert;
    }

    public function setLowStockAlert(?bool $low_stock_alert): self
    {
        $this->low_stock_alert = $low_stock_alert;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getWholesalePrice(): ?float
    {
        return $this->wholesale_price;
    }

    public function setWholesalePrice(?float $wholesale_price): self
    {
        $this->wholesale_price = $wholesale_price;

        return $this;
    }

    public function getUnity(): ?string
    {
        return $this->unity;
    }

    public function setUnity(?string $unity): self
    {
        $this->unity = $unity;

        return $this;
    }

    public function getAdditionalShippingCost(): ?float
    {
        return $this->additional_shipping_cost;
    }

    public function setAdditionalShippingCost(?float $additional_shipping_cost): self
    {
        $this->additional_shipping_cost = $additional_shipping_cost;

        return $this;
    }

    public function getCustomizable(): ?int
    {
        return (int)$this->customizable;
    }

    public function setCustomizable(?int $customizable): self
    {
        $this->customizable = $customizable;

        return $this;
    }

    public function getActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(?bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getRedirectType(): ?string
    {
        return $this->redirect_type;
    }

    public function setRedirectType(?string $redirect_type): self
    {
        $this->redirect_type = $redirect_type;

        return $this;
    }

    public function getIndexed(): ?bool
    {
        return $this->indexed;
    }

    public function setIndexed(?bool $indexed): self
    {
        $this->indexed = $indexed;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->meta_description;
    }

    public function setMetaDescription(?string $meta_description): self
    {
        $this->meta_description = $meta_description;

        return $this;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->meta_keywords;
    }

    public function setMetaKeywords(?string $meta_keywords): self
    {
        $this->meta_keywords = $meta_keywords;

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->meta_title;
    }

    public function setMetaTitle(?string $meta_title): self
    {
        $this->meta_title = $meta_title;

        return $this;
    }

    public function getLinkRewrite(): ?string
    {
        return $this->link_rewrite;
    }

    public function setLinkRewrite(?string $link_rewrite): self
    {
        $this->link_rewrite = $link_rewrite;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescriptionShort(): ?string
    {
        return $this->description_short;
    }

    public function setDescriptionShort(?string $description_short): self
    {
        $this->description_short = $description_short;

        return $this;
    }

    public function getAvailableNow(): ?string
    {
        return $this->available_now;
    }

    public function setAvailableNow(?string $available_now): self
    {
        $this->available_now = $available_now;

        return $this;
    }

    public function getAvailableLater(): ?string
    {
        return $this->available_later;
    }

    public function setAvailableLater(?string $available_later): self
    {
        $this->available_later = $available_later;

        return $this;
    }

    public function __toString()
    {
        return (string)$this->name;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addProduct($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->removeElement($category)) {
            $category->removeProduct($this);
        }

        return $this;
    }

    public function getManufacturer(): ?Manufacturer
    {
        return $this->manufacturer;
    }

    public function setManufacturer(?Manufacturer $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * @return Collection|ProductAttribute[]
     */
    public function getProductAttributes(): Collection
    {
        return $this->productAttributes;
    }

    public function addProductAttribute(ProductAttribute $productAttribute): self
    {
        if (!$this->productAttributes->contains($productAttribute)) {
            $this->productAttributes[] = $productAttribute;
            $productAttribute->setProduct($this);
        }

        return $this;
    }

    public function removeProductAttribute(ProductAttribute $productAttribute): self
    {
        if ($this->productAttributes->removeElement($productAttribute)) {
            // set the owning side to null (unless already changed)
            if ($productAttribute->getProduct() === $this) {
                $productAttribute->setProduct(null);
            }
        }

        return $this;
    }

    public function getSupplier(): ?Supplier
    {
        return $this->supplier;
    }

    public function setSupplier(?Supplier $supplier): self
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * @return Collection|OrderProduct[]
     */
    public function getOrderProducts(): Collection
    {
        return $this->orderProducts;
    }

    public function addOrderProduct(OrderProduct $orderProduct): self
    {
        if (!$this->orderProducts->contains($orderProduct)) {
            $this->orderProducts[] = $orderProduct;
            $orderProduct->setProduct($this);
        }

        return $this;
    }

    public function removeOrderProduct(OrderProduct $orderProduct): self
    {
        if ($this->orderProducts->removeElement($orderProduct)) {
            // set the owning side to null (unless already changed)
            if ($orderProduct->getProduct() === $this) {
                $orderProduct->setProduct(null);
            }
        }

        return $this;
    }

    public function getAvailableStock(ProductAttribute $productAttribute = null)
    {
        $stocks = $this->getStocks()->filter(function (Stock $stock) use ($productAttribute) {
            return $stock->getProductAttribute() == $productAttribute;
        });
        return $stocks->first() ? $stocks->first()->getUsableQuantity() : null;
    }

    /**
     * @return Collection|Stock[]
     */
    public function getStocks(): Collection
    {
        return $this->stocks;
    }

    public function addStock(Stock $stock): self
    {
        if (!$this->stocks->contains($stock)) {
            $this->stocks[] = $stock;
            $stock->setProduct($this);
        }

        return $this;
    }

    public function removeStock(Stock $stock): self
    {
        if ($this->stocks->removeElement($stock)) {
            // set the owning side to null (unless already changed)
            if ($stock->getProduct() === $this) {
                $stock->setProduct(null);
            }
        }

        return $this;
    }

    public function entitySchema()
    {
        return [
            'id',
            'manufacturer' => 'id_manufacturer',
            'supplier' => 'id_supplier',
            'id_category_default',
            'reference', 'supplier_reference', 'location', 'width', 'height', 'depth', 'weight', 'quantity_discount',
            'ean13', 'isbn', 'upc', 'mpn', 'cache_is_pack', 'is_virtual', 'state', 'additional_delivery_times',
            'delivery_in_stock', 'delivery_out_stock', 'on_sale', 'online_only', 'ecotax', 'minimal_quantity',
            'low_stock_threshold', 'low_stock_alert', 'price', 'wholesale_price', 'unity', 'additional_shipping_cost',
            'customizable', 'active', 'redirect_type', 'indexed', 'meta_description', 'meta_keywords', 'meta_title',
            'link_rewrite', 'name', 'description', 'description_short', 'available_now', 'available_later',
        ];
    }

    static public function getResourceName()
    {
        return "product";
    }
}
