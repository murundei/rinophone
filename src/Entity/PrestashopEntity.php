<?php

namespace App\Entity;

use App\Traits\TextFormatter;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Doctrine\Common\Annotations\AnnotationReader;
use Exception;
use ReflectionException;

abstract class PrestashopEntity
{
    use TextFormatter;

    /** @ORM\Column(type="integer", unique=true) */
    protected $id_entity;

    /** @ORM\Column(type="datetime", nullable=true) */
    protected $date_add;

    /** @ORM\Column(type="datetime", nullable=true) */
    protected $date_upd;

    /**
     * Entity schema. Fields which using from wevbservice.
     *
     * @return mixed
     */
    abstract public function entitySchema();


    abstract static public function getResourceName();

    /**
     * @param $relations
     * @return array
     */
    public function serialize($relations)
    {
        $data = [];
        $relationsData = [];

        foreach ($this as $key => $value) {
            if (!$value) {
                $data['data'][$key] = '';
            } else {
                $data['data'][$key] = $value;
            }

            if (in_array($key, array_keys($relations))) {
                if ($relations[$key]['type'] == 8) {
                    $getMethod = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));
                    foreach ($this->$getMethod() as $relationItem) {
                        $relationsData[$key][] = $relationItem->getIdEntity();
                    }
                }
            }
        }

        foreach ($relations as $relation) {
            if ($relation['type'] == 2) {
                $field = $relation['fieldName'];

                $getMethod = 'get' . str_replace(' ', '', ucwords(str_replace('_', ' ', $field)));

                if (method_exists($this, $getMethod)) {
                    if ($this->{$getMethod}()) {
                        $data['data']['id_' . $this->camelToSnakeCase($field)] = $this->$getMethod()->getIdEntity();
                    }
                }
            }
        }

        $data['id'] = $this->id_entity;
        $data['associations'] = $relationsData;

        return $data;
    }

    /**
     * Unserialize xml to object.
     *
     * @param $array
     * @return $this
     * @throws ReflectionException
     * @throws Exception
     */
    public function unserialize($array): self
    {
        $reflect = new \ReflectionClass($this);
        $docReader = new AnnotationReader();

        $array = (object)$array;

        foreach ($array as $key => $value) {
            if (is_array($value) && empty($value) && $key != 'associations') {
                $value = null;
            }

            if ($key == "id") {
                $this->id_entity = (int)$value;
            } elseif (property_exists(get_class($this), $key)) {
                $docInfos = $docReader->getPropertyAnnotations($reflect->getProperty($key));
                $type = null;

                if (isset($docInfos[0]) && property_exists($docInfos[0], "type"))
                    $type = ($docInfos[0]->type);

                if (isset($value->language)) {
                    $this->$key = $this->prepareValue($value['language'][0], $type);
                } else {
                    $func_name = $this->generateSetterMethod($key);
                    $this->$func_name($this->prepareValue($value, $type));
                }
            }

            if ($key == 'associations') {
                foreach ($value as $associate => $array) {
                    $this->$key[$associate] = $array;
                }
            }

            if ($key == 'date_add') {
                if ((int)(new DateTime($array->$key))->format('Y-m-d h:i:s') > 0) {
                    $this->$key = new DateTime($array->$key);
                }
            } elseif ($key == 'date_upd') {
                $this->$key = new DateTime;
            }

        }

        if (!$this->date_add) {
            $this->date_add = new DateTime;
        }

        if (!$this->date_upd) {
            $this->date_upd = new DateTime;
        }

        return $this;
    }

    protected function generateSetterMethod(string $field_name)
    {
        $func_name = "set" . str_replace(' ', '', ucwords(str_replace('_', ' ', $field_name)));
        return $func_name;
    }

    /**
     * Prepare value to serialize or unserialize.
     *
     * @param $value
     * @param $type
     * @return bool|DateTime|float|int|string|null
     * @throws Exception
     */
    protected function prepareValue($value, $type)
    {
        if ($type == "date" || $type == "datetime") {
            $due = strtotime($value);
            if ($due <= 0) {
                return null;
            }
            return new \DateTime($value);
        } elseif ($type == "string") {
            return (string)$value;
        } elseif ($type == 'boolean') {
            return (bool)$value;
        } elseif ($type == 'int') {
            return (int)$value;
        } elseif ($type == 'float') {
            return (float)$value;
        } elseif ($type == 'double') {
            return (double)$value;
        } else {
            return $value;
        }

    }

    /**
     * @return int|null
     */
    public function getIdEntity(): ?int
    {
        return $this->id_entity;
    }

    /**
     * @param int $id_entity
     * @return $this
     */
    public function setIdEntity(int $id_entity): self
    {
        $this->id_entity = $id_entity;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDateAdd(): ?DateTimeInterface
    {
        return $this->date_add;
    }

    /**
     * @param DateTimeInterface|null $date_add
     * @return $this
     */
    public function setDateAdd(?DateTimeInterface $date_add): self
    {
        $this->date_add = $date_add;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDateUpd(): ?DateTimeInterface
    {
        return $this->date_upd;
    }

    /**
     * @param DateTimeInterface|null $date_upd
     * @return $this
     */
    public function setDateUpd(?DateTimeInterface $date_upd): self
    {
        $this->date_upd = $date_upd;

        return $this;
    }
}
