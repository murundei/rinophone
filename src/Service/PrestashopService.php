<?php

namespace App\Service;

use App\Entity\PrestashopEntity;
use Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Yaml\Yaml;
use Webservice;

class PrestashopService
{
    protected $parameterBag;
    protected $em;
    protected $container;
    private $configDir;

    /**
     * PrestashopService constructor.
     *
     * @param ParameterBagInterface $parameterBag
     * @param EntityManagerInterface $em
     */
    public function __construct(ParameterBagInterface $parameterBag, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->parameterBag = $parameterBag;
        $this->configDir = $this->parameterBag->get("config.dir");
    }

    /**
     * Prestashop webservice.
     *
     * @return string
     */
    public function webService()
    {
        return (new Webservice($this->parameterBag->get('PSWEB360WS_URL'), $this->parameterBag->get('PSWEB360WS_PASSWORD')));
    }

    /**
     * @param $entity
     * @param $id_entity
     * @return mixed
     * @throws Exception
     */
    public function loadEntity($entity, $id_entity)
    {
        $em = $this->em;

        $resource = $entity::getResourceNamePlural();

        $xml = $this->webService()->get([
            'resource' => $resource,
            'id' => $id_entity
        ]);

        $entityXML = $xml->{$entity::getResourceNameSingular()}->children();
        $entity = new $entity;

        $entity->unserialize($entityXML);

        $em->persist($entity);

        $em->flush();

        return $entity;
    }

    /**
     * Update one entity
     * @param $entity
     * @param $xml
     * @return object|null
     */
    public function updateEntity($entity, $xml)
    {
        $entity = $this->em->getRepository($entity)->findOneBy([
            'id_entity' => $xml->id
        ]);

        $entity->unserialize($xml);

        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }

    /**
     * Load one resource referring from the entity class.
     *
     * @param $className
     * @throws Exception
     */
    public function loadResource($className)
    {
        $em = $this->em;

        $resource = $className::getResourceName();

        $offset = 0;

        do {
            $entities = $this->webService()->get([
                'resource' => $resource,
                'display' => 'full',
                'limit' => '100,' . $offset,
            ]);

            if ($entities) {
                foreach ($entities[$resource] as $entityArray) {
                    $entity = $em->getRepository($className)->findOneBy([
                        'id_entity' => $entityArray['id']
                    ]);

                    if (!$entity) {
                        $entity = new $className;
                    }

                    $entity->unserialize($entityArray);

                    $em->persist($entity);
                }
            }

            $em->flush();
            $offset += 100;
        } while ($entities);
    }

    /**
     * Load all resources.
     */
    public function loadAllResources()
    {
        $fields = Yaml::parseFile($this->configDir . '/entities.yaml')['prestashop']['sync_all_fields'];
        foreach ($fields as $class) {
            $this->loadResource($class);
        }
    }

    /**
     * Update one resource .
     *
     * @param PrestashopEntity[] $entityArray
     * @throws Exception
     */
    public function updateResource(array $entityArray)
    {
        foreach ($entityArray as $item) {
            $relations = $this->em->getMetadataFactory()->getMetadataFor(get_class($item))->associationMappings;
            $data = $item->serialize($relations);

            $this->webService()->edit([
                'resource' => $item::getResourceName(),
                'id' => $item->getIdEntity(),
                'data' => $data
            ]);
        }
    }

    /**
     * Update all resources.
     */
    public function updateAllResources()
    {
        $fields = Yaml::parseFile($this->configDir . '/entities.yaml')['prestashop']['sync_all_fields'];

        foreach ($fields as $class) {
            $rows = $this->em->getRepository($class)->findAll();

            $this->updateResource($rows);
        }
    }

    public function removeResource($entity, $id)
    {
        $this->webService()->delete([
            'resource' => $entity,
            'id' => $id
        ]);
    }
}
