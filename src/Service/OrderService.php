<?php


namespace App\Service;


use App\Entity\Order;
use App\Entity\PrestashopOrder;
use App\Entity\PrestashopOrderDetail;
use App\Entity\Product;
use App\Entity\Stock;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;

class OrderService
{
    private $em;
    private $configDir;
    private $configOrder;
    private $parameterBag;
    private $prestashopService;

    public function __construct(EntityManagerInterface $em, ParameterBagInterface $parameterBag,
                                PrestashopService $prestashopService)
    {
        $this->em = $em;
        $this->parameterBag = $parameterBag;
        $this->prestashopService = $prestashopService;

        $this->configDir = $this->parameterBag->get("config.dir");
        $this->configOrder = Yaml::parseFile($this->configDir . '/prestashop_order.yaml');
    }

    public function checkOrders()
    {
        $prestashopService = $this->prestashopService;
        $hourAgo = urlencode(date('Y-m-d H:i:s', time() - 3600));
        $now = urlencode(date('Y-m-d H:i:s'));

        $xml = $prestashopService->webService(true)->get([
            'resource' => 'orders?filter[date_upd]=' . '["' . $hourAgo . '","' . $now . '"]&date=1&display=[id]',
        ]);

        $orders = $xml->orders->children();

        $ordersCount = 0;

        if ($orders) {
            foreach ($orders as $order) {
                $ordersCount++;
                $localOrder = $this->em->getRepository(Order::class)
                    ->findOneBy([
                        'id_entity' => $order->id
                    ]);

                ($localOrder) ?
                    $prestashopService->updateEntity(Order::class, $order) :
                    $prestashopService->loadEntity(Order::class, $order->id);

                $orderRows = $order->associations->order_rows->order_row;

                $newOrder = false;
                $orderDetails = [];
                $orderPrice = 0;
                $supplier = null;

                if ($orderRows) {
                    foreach ($orderRows as $orderRow) {
                        $productId = $orderRow->product_id;

                        $stocks = $this->em->getRepository(Stock::class)->findBy([
                            'id_product' => $productId
                        ]);

                        /** @var Stock $stock */
                        foreach ($stocks as $stock) {
                            $quantity = $stock->getQuantity();

                            /** @var Product $product */
                            $product = $stock->getProduct();

                            $orderPrice += $product->getPrice();

                            $supplier = $product->getSupplier();

                            $orderDetails[] = [
                                'product' => $product,
                                'price' => $product->getPrice(),
                                'quantity' => $quantity

                            ];

                            if ($quantity < 0) {
                                $newOrder = true;
                            }
                        }

                    }
                }

                if ($newOrder) {
                    $order = $this->createNewOrder($supplier, $orderPrice);

                    foreach ($orderDetails as $orderDetail) {
                        $product = $this->em->getRepository(Product::class)
                            ->findOneBy([
                                'id_entity' => $orderDetail
                            ]);

                        $orderDetail = new PrestashopOrderDetail;
                        $orderDetail->setQuantity($orderDetail['quantity']);
                        $orderDetail->setPrice($orderDetail['price']);
                        $orderDetail->getProduct($orderDetail['product']);
                        $orderDetail->setPrestashopOrder($order);

                        $this->em->persist($orderDetail);
                    }
                }
            }
        }

        $this->em->flush();

        return $ordersCount > 0 ? 'Successfully added ' . $ordersCount . ' orders' : 'No orders found';
    }

    private function createNewOrder($supplier, $orderPrice)
    {
        $prestashopOrder = new PrestashopOrder();
        $prestashopOrder->setDate(new \DateTime());
        $prestashopOrder->setSupplier($supplier);
        $year = substr(date("y"), -2);
        $month = date("m");
        $prestashopOrder->setPrice($orderPrice);
        $lastOrderId = (int)$this->configOrder["order"]["last_id"];
        $prestashopOrder->setReference("CO" . $year . "-" . $month . sprintf("%02d", $lastOrderId));
        $this->configOrder["order"]["last_id"]++;
        file_put_contents($this->configDir . '/prestashop_order.yaml', Yaml::dump($this->configOrder));

        $this->em->persist($prestashopOrder);

        return $prestashopOrder;
    }

}