<?php

namespace App\Repository;

use App\Entity\OfflineTax;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OfflineTax|null find($id, $lockMode = null, $lockVersion = null)
 * @method OfflineTax|null findOneBy(array $criteria, array $orderBy = null)
 * @method OfflineTax[]    findAll()
 * @method OfflineTax[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfflineTaxRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OfflineTax::class);
    }

    // /**
    //  * @return OfflineTax[] Returns an array of OfflineTax objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OfflineTax
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
