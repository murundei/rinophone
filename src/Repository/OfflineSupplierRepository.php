<?php

namespace App\Repository;

use App\Entity\OfflineSupplier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OfflineSupplier|null find($id, $lockMode = null, $lockVersion = null)
 * @method OfflineSupplier|null findOneBy(array $criteria, array $orderBy = null)
 * @method OfflineSupplier[]    findAll()
 * @method OfflineSupplier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OfflineSupplierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OfflineSupplier::class);
    }

    // /**
    //  * @return OfflineSupplier[] Returns an array of OfflineSupplier objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OfflineSupplier
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
