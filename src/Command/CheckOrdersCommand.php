<?php

namespace App\Command;

use App\Service\OrderService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CheckOrdersCommand extends Command
{
    protected static $defaultName = 'app:check-orders';
    private $orderService;

    public function __construct(OrderService $orderService)
    {
        parent::__construct();

        $this->orderService = $orderService;

    }

    protected function configure()
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Run to check a new orders...')
            ->setHelp('This command allows you to check new orders (created/updated during the last 1 hour).');

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $commandResponse = $this->orderService->checkOrders();

        $io->success($commandResponse);

        return Command::SUCCESS;
    }
}
