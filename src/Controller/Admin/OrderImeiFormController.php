<?php


namespace App\Controller\Admin;



use App\Entity\Order;
use App\Entity\OrderProduct;
use App\Entity\PhoneSupply;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderImeiFormController extends AbstractController
{
    /**
     * @param Request $request
     * @return Response
     * @Route("/order_imei_form", name="order_imei_form")
     */
    public function index( Request $request)
    {
        return $this->render('admin/order_imei_form/index.html.twig',[
        ]);
    }

    /**
     * Set imeis for order product
     *
     * @Route("/setIMEIs/{orderProduct}")
     * @param OrderProduct $orderProduct
     * @param Request $request
     * @return Response
     */
    public function setOrderProductIMEIs(OrderProduct $orderProduct, Request $request)
    {
        $imeiString = $request->request->get("imei");
        $imeis = array_filter(explode(",", $imeiString), function ($item) {
            return $item;
        });
        if (!$imeis) return new Response("Empty", 400);
        $entityManager = $this->getDoctrine()->getManager();
        $invalidImeis = [];
        foreach ($imeis as $ind => $imei) {
            $phoneSupply = $entityManager->getRepository(PhoneSupply::class)->findOneBy(["imei" => $imei]);
            if (!$phoneSupply) {
                array_push($invalidImeis, $imei);
                continue;
            }
            $orderProduct->setPhoneSupply($phoneSupply, $ind);
            $entityManager->persist($orderProduct);
        }
        $entityManager->flush();
        if (!empty($invalidImeis)) {
            return new Response("There no such phone supplies: ". join(",", $invalidImeis), 404);
        }

        return new Response("Success");
    }

    /**
     * Set imeis for order
     *
     * @Route("/setOrderIMEIs/{orderRef}")
     * @param string $orderRef
     * @param Request $request
     * @return Response
     */
    public function setOrderIMEIs($orderRef, Request $request)
    {
        $imeiString = $request->request->get("imei");
        $imeis = array_filter(explode(",", $imeiString), function ($item) {
            return $item;
        });
        if (!$imeis) return new Response("Empty", 400);
        $entityManager = $this->getDoctrine()->getManager();
        $order = $entityManager->getRepository(Order::class)->findOneBy(["reference"=>$orderRef]);
        if (!$order) return new Response("There no such order", 404);

        $invalidImeis = [];
        $suppliesAttached = 0;
        $orderProducts = $order->getOrderProducts();
        $order->clearPhoneSupplies();
        $orderProductInd = 0;
        foreach ($imeis as $ind => $imei) {
            $phoneSupply = $entityManager->getRepository(PhoneSupply::class)->findOneBy(["imei" => $imei]);
            if (!$phoneSupply) {
                array_push($invalidImeis, $imei);
                continue;
            }
            $orderProducts[$orderProductInd]->addPhoneSupply($phoneSupply);
            $suppliesAttached++;
            if($orderProducts[$orderProductInd]->getProductQuantity()==$suppliesAttached){
                $suppliesAttached=0;
                $orderProductInd++;
            }
            $entityManager->persist($orderProducts[$orderProductInd]);

        }
        $entityManager->persist($order);

        $entityManager->flush();
        if (!empty($invalidImeis)) {
            return new Response("There no such phone supplies: ". join(",", $invalidImeis), 404);
        }

        return new Response("Success");
    }

    /**
     * Set imei for order
     *
     * @Route("/setOrdersIMEI")
     * @param string $orderRef
     * @param Request $request
     * @return Response
     */
    public function setOrdersIMEI(Request $request)
    {
        $imeiString = $request->request->get("refImei");
        if (!$imeiString) return new Response("Empty", 400);
        $imeis = json_decode($imeiString,true);
        $entityManager = $this->getDoctrine()->getManager();
        foreach ($imeis as $imeiRow){
            /* @var Order $order*/
            $order = $entityManager->getRepository(Order::class)->findOneBy(["reference"=>$imeiRow["ref"]]);
            if (!$order) return new Response("There no such order: ".$imeiRow["ref"], 404);
            /* @var OrderProduct $orderProduct*/
            $orderProduct=$order->getOrderProducts()->first();
            $orderProduct->clearPhoneSupplies();
            /* @var PhoneSupply $phoneSupply*/
            $phoneSupply = $entityManager->getRepository(PhoneSupply::class)->findOneBy(["imei" => $imeiRow["imei"]]);
            if (!$phoneSupply) return new Response("There no such phone supply: ". $imeiRow["imei"], 404);
            $orderProduct->addPhoneSupply($phoneSupply);
            $entityManager->persist($orderProduct);
        }
        $entityManager->flush();
        return new Response("Success");
    }
}