<?php


namespace App\Controller\Admin;

use App\Entity\Order;
use App\Entity\Carrier;
use App\Entity\OrderProduct;
use App\Entity\OrderState;
use App\Entity\PhoneSupply;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends EasyAdminController
{

    /**
     * Change order carrier
     *
     * @param int $orderId Order to be changed
     * @param int $carrierId New order carrier
     * @Route("/updateOrderCarrier/{orderId}/{carrierId}", name="updateOrderCarrier")
     * @return string|RedirectResponse
     */
    public function updateOrderCarrier(int $orderId, int $carrierId)
    {

        $entityManager = $this->getDoctrine()->getManager();

        /** @var Order $order */
        $order = $entityManager->getRepository(Order::class)->find($orderId);
        $order->setIdCarrier($carrierId);

        /** @var Carrier $carrier */
        $carrier = $entityManager->getRepository(Carrier::class)->find($carrierId);

        $order->setCarrier($carrier);

        $entityManager->persist($order);
        $entityManager->flush();

        return new Response('Carrier changed');
    }

    /**
     * Change order weight
     *
     * @param int $orderId Order to be changed
     * @param string $weight New order weight
     * @Route("/updateOrderWeight/{orderId}/{weight}", name="updateOrderWeight")
     * @return string|RedirectResponse
     */
    public function updateOrderWeight(int $orderId, string $weight)
    {

        $entityManager = $this->getDoctrine()->getManager();

        $order = $entityManager->getRepository(Order::class)->find($orderId);
        $weight = floatval($weight);
        $order->setOrderWeight($weight);

        $entityManager->persist($order);
        $entityManager->flush();

        return new Response('Weight changed');
    }

    /**
     * Get all Carriers
     *
     * @Route("/getCarries", name="get_carriers")
     * @return string|RedirectResponse
     */
    public function getCarries()
    {

        $entityManager = $this->getDoctrine()->getManager();

        /* @var Carrier[] $carries */
        $carries = $entityManager->getRepository(Carrier::class)->findAll();
        $carriesArray = [];
        foreach ($carries as $carrier) {
            array_push($carriesArray, [
                "id" => $carrier->getId(),
                "name" => $carrier->getName()
            ]);
        }

        return new Response(json_encode($carriesArray));
    }


    /**
     * Get all Carriers
     *
     * @Route("/getStates", name="get_states")
     * @return Response
     */
    public function getStates()
    {
        $entityManager = $this->getDoctrine()->getManager();

        /* @var OrderState[] $states */
        $states = $entityManager->getRepository(OrderState::class)->findAll();
        $statesArray = [];
        foreach ($states as $state) {
            array_push($statesArray, [
                'id' => $state->getId(),
                'name' => $state->getName(),
                'color' => $state->getColor()
            ]);
        }

        return new Response(json_encode($statesArray));
    }

    /**
     * Change order state
     *
     * @Route("/updateOrderState/{order}/{state}")
     * @param Order $order
     * @param OrderState $state
     * @return Response
     */
    public function updateOrderState(Order $order, OrderState $state)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $order->setOrderState($state);
        $entityManager->persist($order);

        $entityManager->flush();

        return new Response($state->getColor());
    }

    protected function createListQueryBuilder($entityClass, $sortDirection, $sortField = null, $dqlFilter = null)
    {
        if ($this->entity['class'] !== Order::class) return parent::createListQueryBuilder($entityClass, $sortDirection, $sortField, $dqlFilter);

        /* @var QueryBuilder */
        $queryBuilder = $this->em->createQueryBuilder()
            ->select('entity')
            ->from($this->entity['class'], 'entity')
            ->leftJoin('entity.orderState', 'orderState');

        if (!empty($dqlFilter)) {
            $queryBuilder->andWhere($dqlFilter);
        }

        if (null !== $sortField) {
            $queryBuilder->orderBy('entity.' . $sortField, $sortDirection ?: 'DESC');
        }

        return $queryBuilder;
    }

}