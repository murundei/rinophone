<?php
namespace App\Controller\Admin;


use App\Entity\OfflineSupplier;
use App\Entity\OfflineTax;
use App\Entity\PhoneSupply;
use App\Entity\Supplier;
use App\Form\PhoneSupplyType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PhoneSupplyController extends AbstractController
{
    /**
     * @param Request $request
     * @return Response
     * @Route("/phone_supply", name="phone_supply")
     */
    public function index( Request $request)
    {
        $form = $this->createForm(PhoneSupplyType::class);
        $this->handlePhoneSupplyForm($form,$request);
        $em = $this->getDoctrine()->getManager();
        $phoneSupplies = $em->getRepository(PhoneSupply::class)->findBy(array(), array('id' => 'DESC'));;
        $offlineSuppliers = $em->getRepository(OfflineSupplier::class)->findAll();
        $offlineTaxes = $em->getRepository(OfflineTax::class)->findAll();;

        return $this->render('admin/phone_supply/index.html.twig',[
            'phone_supplies'=>$phoneSupplies,
            'suppliers'=>$offlineSuppliers,
            'taxes'=>$offlineTaxes,
            'form_supply'=>$form->createView(),
        ]);
    }

    /**
     * @param FormInterface $form
     * @param Request $request
     * @return null|PhoneSupply
     */
    protected function handlePhoneSupplyForm(FormInterface $form, Request$request): ?PhoneSupply
    {
        $entityManager=$this->getDoctrine()->getManager();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $count=$form->get("count")->getData();
            /* @var PhoneSupply $phoneSupply*/
            $phoneSupply = $form->getData();
            $phoneSupply->setDateAdd(new \DateTime());
            for($i=0;$i<$count;$i++){
                $new = clone $phoneSupply;
                $entityManager->persist($new);
            }
            $entityManager->flush();
            return $phoneSupply;
        }
        return null;
    }

    /**
     * @param PhoneSupply $phoneSupply
     * @param $field
     * @param Request $request
     * @return Response
     * @Route("/phone_supply/save_value/{phoneSupply}/{field}", name="phone_supply.save_value")
     */
    public function saveValue(PhoneSupply $phoneSupply, $field, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $value = $request->request->get("value");
        if(!$value) return new Response("Failed",400);
        switch ($field){
            case "name":$phoneSupply->setName($value);break;
            case "imei":$phoneSupply->setImei($value);break;
            case "invoiceNum":$phoneSupply->setInvoiceNum($value);break;
            case "price":$phoneSupply->setPrice($value);break;
            case "supplier":
                $supplier=$em->getRepository(OfflineSupplier::class)->find((int)$value);
                $phoneSupply->setSupplier($supplier);
                break;
            case "tax":
                $tax=$em->getRepository(OfflineTax::class)->find((int)$value);
                $phoneSupply->setTax($tax);
                break;
            case "dateDelivery":
                $date = new \DateTime($value);
                $phoneSupply->setDateDelivery($date);
                break;
            case "dateInvoice":
                $date = new \DateTime($value);
                $phoneSupply->setDateInvoice($date);
                break;
        }
        $em->persist($phoneSupply);
        $em->flush();
        return new Response("Success");
    }
}