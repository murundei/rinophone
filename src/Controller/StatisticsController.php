<?php

namespace App\Controller;

use App\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StatisticsController extends AbstractController
{
    /**
     * @Route("/statistics", name="statistics")
     */
    public function index(): Response
    {
        return $this->render('statistics/index.html.twig', [
            'controller_name' => 'StatisticsController',
        ]);
    }

    /**
     * @Route("/piestat", name="piestat")
     */
    public function pieStat(Request $request): Response
    {
        $start = new \DateTime($request->request->get("start"));
        $end = new \DateTime($request->request->get("end"));
        $orders = $this->getDoctrine()->getManager()->getRepository(Order::class)->findByDateRange($start, $end);
        $sumCost = 0;
        $income = [];
        $income["prestashop"] = 0;
        $incomeSum=0;
        $marketPlaceSum =[];
        foreach ($orders as $order) {
            $sumTaxOrder = $order->getTotalPaidTaxIncl() - $order->getTotalPaidTaxExcl();
            $incomeOrder = 0;

            foreach ($order->getOrderProducts() as $orderProduct) {
                $orderProductIncome = 0;
                $totalPrice = $orderProduct->getUnitPriceTaxIncl();
                $phoneSupplies = 0;
                if ($orderProduct->getPhoneSupplies()) {
                    foreach ($orderProduct->getPhoneSupplies() as $phoneSupply) {
                        if ($phoneSupply->getTax()) {
                            $wholePrice = $phoneSupply->getPrice();
                            $tax = ($totalPrice - $wholePrice) * $phoneSupply->getTax()->getPercent() / 100;
                            $orderProductIncome += $totalPrice - $wholePrice - $tax;
                        }
                    }
                }
                if ($phoneSupplies < $orderProduct->getProductQuantity()) {
                    $wholePrice = $orderProduct->getProductAttribute() ?
                        $orderProduct->getProductAttribute()->getWholesalePrice() :
                        $orderProduct->getProduct()->getWholesalePrice();
                    $resQuantity = $orderProduct->getProductQuantity() - $phoneSupplies;
                    $orderProductIncome += ($orderProduct->getUnitPriceTaxExcl() - $wholePrice) * $resQuantity;
                }
                $incomeOrder += $orderProductIncome;
            }


            if ($order->getOrderState() && count($order->getOrderState()->getmarketPlaceCosts())) {
                $mp = $order->getOrderState()->getmarketPlaceCosts()[0];
                $marketPlaceCost =
                    $order->getTotalPaidTaxIncl() - $mp->getDeliveryCost() - $mp->getWorkCost()
                    - ($order->getTotalPaidTaxIncl() * $mp->getMarketplacePercent() / 100);
                $incomeOrder-=$marketPlaceCost;
                if(!isset($income[$mp->getName()]))
                    $income[$mp->getName()]=$incomeOrder;
                else $income[$mp->getName()]+=$incomeOrder;

                if(!isset($marketPlaceSum[$mp->getName()]))
                    $marketPlaceSum[$mp->getName()]=$marketPlaceCost;
                else $marketPlaceSum[$mp->getName()]+=$marketPlaceCost;

                $sumCost+=$marketPlaceCost;
            }
            else{
                $income["prestashop"]+=$incomeOrder;
            }
            $incomeSum+=$incomeOrder;
        }
        return $this->render('statistics/diagram.html.twig', [
            'start' => $start,
            'end' => $end,
            'marketPlaceSum' =>$marketPlaceSum,
            'sumCost'=>$sumCost,
            'incomeSum'=>$incomeSum,
            'income'=>$income
            ]);
    }


    /**
     * @Route("/countTax", name="count_tax")
     */
    public function countTax(Request $request): Response
    {
        $start = new \DateTime($request->request->get("start"));
        $end = new \DateTime($request->request->get("end"));
        $orders = $this->getDoctrine()->getManager()->getRepository(Order::class)->findByDateRange($start, $end);
        $taxes = [];
        $income = [];
        $customerPaid = [];
        $marketPlaceCost = [];
        $taxSum =0;
        $customerPaidSum = 0;
        $marketPlaceSum =0;
        foreach ($orders as $order) {
            $taxes[$order->getIdEntity()] = 0;
            $income[$order->getIdEntity()] = 0;
            $customerPaid[$order->getIdEntity()] = $order->getTotalPaidTaxIncl();
            $marketPlaceCost[$order->getIdEntity()] = 0;
            if ($order->getOrderState() && count($order->getOrderState()->getmarketPlaceCosts())) {
                $mpCost = $order->getOrderState()->getmarketPlaceCosts()[0];
                $marketPlaceCost[$order->getIdEntity()] =
                    $order->getTotalPaidTaxIncl() - $mpCost->getDeliveryCost() - $mpCost->getWorkCost()
                    - ($order->getTotalPaidTaxIncl() * $mpCost->getMarketplacePercent() / 100);
            }
            foreach ($order->getOrderProducts() as $orderProduct) {
                $tax = 0;
                $orderProductIncome = 0;
                $totalPrice = $orderProduct->getUnitPriceTaxIncl();
                $phoneSupplies = 0;
                if ($orderProduct->getPhoneSupplies()) {
                    foreach ($orderProduct->getPhoneSupplies() as $phoneSupply) {
                        if ($phoneSupply->getTax()) {
                            $wholePrice = $phoneSupply->getPrice();
                            $taxSupply= ($totalPrice - $wholePrice) * $phoneSupply->getTax()->getPercent() / 100;
                            $orderProductIncome += $totalPrice - $wholePrice - $taxSupply;
                            $tax+=$taxSupply;
                            $phoneSupplies++;
                        }
                    }

                }
                if ($phoneSupplies < $orderProduct->getProductQuantity()) {
                    $wholePrice = $orderProduct->getProductAttribute() ?
                        $orderProduct->getProductAttribute()->getWholesalePrice() :
                        $orderProduct->getProduct()->getWholesalePrice();
                    $resQuantity = $orderProduct->getProductQuantity() - $phoneSupplies;
                    $orderProductIncome += ($orderProduct->getUnitPriceTaxExcl() - $wholePrice) * $resQuantity;
                    $tax += $resQuantity * $orderProduct->getUnitPriceTaxIncl() - $orderProduct->getUnitPriceTaxExcl();
                }
                $taxes[$order->getIdEntity()] += $tax;
                $income[$order->getIdEntity()] += $orderProductIncome;
            }
            $taxSum+=$taxes[$order->getIdEntity()];
            $incomeSum+=$income[$order->getIdEntity()];
            $marketPlaceSum+=$marketPlaceCost[$order->getIdEntity()];
            $customerPaidSum+=$customerPaid[$order->getIdEntity()];
        }
        return $this->render('statistics/index.html.twig', [
            'start' => $start,
            'end' => $end,
            'taxes' => $taxes,
            'income' => $income,
            'customerPaid' => $customerPaid,
            'marketPlaceCost' =>$marketPlaceCost,
            'taxSum'=>$taxSum,
            'incomeSum'=>$incomeSum,
            'customerPaidSum'=>$customerPaidSum,
            'marketPlaceCostSum'=>$marketPlaceSum,
        ]);
    }
}
