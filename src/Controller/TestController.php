<?php

namespace App\Controller;

use App\Entity\Manufacturer;
use App\Service\PrestashopService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends AbstractController
{
    private $em;
    private $parameterBag;

    public function __construct(EntityManagerInterface $em, ParameterBagInterface $parameterBag)
    {
        $this->em = $em;
        $this->parameterBag = $parameterBag;
    }

    /**
     * @Route("/load")
     * @param PrestashopService $prestashopService
     * @param Request $request
     * @return RedirectResponse
     * @throws Exception
     */
    public function test(PrestashopService $prestashopService, Request $request)
    {
        $prevUri = $request->headers->get('referer');

        $prestashopService->loadResource(Manufacturer::class);

        return !$prevUri ? $this->redirect('/') : $this->redirect($prevUri);
    }

    /**
     * @Route("/loadAll")
     * @param PrestashopService $prestashopService
     * @param Request $request
     * @return RedirectResponse
     */
    public function test2(PrestashopService $prestashopService, Request $request)
    {
        $prevUri = $request->headers->get('referer');

        $prestashopService->loadAllResources();

        return !$prevUri ? $this->redirect('/') : $this->redirect($prevUri);
    }

    /**
     * @Route("/updateAll")
     * @param PrestashopService $prestashopService
     * @param Request $request
     * @return RedirectResponse
     */
    public function test3(PrestashopService $prestashopService, Request $request)
    {
        $prevUri = $request->headers->get('referer');

        $prestashopService->updateAllResources();

        return !$prevUri ? $this->redirect('/') : $this->redirect($prevUri);
    }
}