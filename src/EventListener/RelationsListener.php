<?php

namespace App\EventListener;

use App\Entity\Category;
use App\Entity\PrestashopEntity;
use App\Entity\Product;
use App\Traits\TextFormatter;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RelationsListener implements EventSubscriberInterface
{
    use TextFormatter;

    private $parameterBag;
    private $em;

    public function __construct(ParameterBagInterface $parameterBag, EntityManagerInterface $em)
    {
        $this->parameterBag = $parameterBag;
        $this->em = $em;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    private function checkRelation(LifecycleEventArgs $args)
    {
        $obj = $args->getObject();
        if (!is_subclass_of($obj, PrestashopEntity::class)) return;
        $class = get_class($obj);
        $relations = $this->em->getMetadataFactory()->getMetadataFor($class)->associationMappings;
        if ($class == Product::class) dd($relations);

        if (isset($obj->associations)) {
            $associations = $obj->associations;

            foreach ($associations as $associate => $resource) {
                $associationIds = $resource[$associate];

                foreach ($associationIds as $id) {
                    if ($class === Product::class && $associate == 'category') {
                        $newRelation = $this->em->getRepository(Category::class)->findOneBy([
                            'id_entity' => $id
                        ]);

                        if ($newRelation) {
                            if (method_exists($obj, 'addCategory')) {
                                $obj->addCategory($newRelation);
                            }
                        }
                    }
                }
            }
        }

        foreach ($obj->entitySchema() as $key => $value) {
            $getMethod = 'get' . $this->snakeToCamelCase($key);

            if (method_exists($obj, $getMethod)) {

                $relationClass = $relations[$key]["targetEntity"];
                if (method_exists($obj, 'get' . $this->snakeToCamelCase($value))) {
                    $value = $obj->{'get' . $this->snakeToCamelCase($value)}();

                    if (class_exists($relationClass)) {
                        $newRelation = $this->em->getRepository($relationClass)->findOneBy([
                            'id_entity' => $value
                        ]);

                        if ($newRelation) {
                            $setMethod = 'set' . $this->snakeToCamelCase($key);
                            if (method_exists($obj, $setMethod)) {
                                $obj->$setMethod($newRelation);
                            }
                        }
                    }
                }
            }
        }
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $this->checkRelation($args);
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $this->checkRelation($args);
    }

    public static function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate
        ];
    }
}
